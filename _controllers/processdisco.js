const alphaAlgo = require('../processmining/alphaAlgo');
const alphaPlusAlgo = require('../processmining/alphaPlusAlgo');
const enhancement = require('../_controllers/enhancement');
const projectManager = require('../lib/projectManager');

var useAlpha = function (Log) {

    // Step 1: Determine each activity in L corresponding to a transition in L.
    var [TL, footprint, oneLoopTransitions] = alphaPlusAlgo.getTransitionsFootPrint(Log);
    console.info("\n\n footprint : " + JSON.stringify(footprint));
    console.info("\n\n transitions : " + JSON.stringify(TL));
    console.info("\n\n oneLoopTransitions : " + JSON.stringify(oneLoopTransitions));

    // Step 2: Fix the set of start activities –that is, the firstelements of each trace:
    // Step 3: Fix the set of end activities –that is, elements that appear lastin a trace
    var [firstTrans, lastTrans] = alphaPlusAlgo.getFirstLastTransitions(footprint);
    console.info("\n\n firstTrans : " + JSON.stringify(firstTrans));
    console.info("\n\n lastTrans : " + JSON.stringify(lastTrans));

    // Step 4: Calculate pairs (A, B)
    var XL = alphaPlusAlgo.findPairsOfSets(footprint, firstTrans[0], lastTrans[0]);
    console.info("\n\n XL pairs : " + JSON.stringify(XL));

    // Step 5: Delete non-maximal pairs (A, B)
    XL.removeNonMaxPairs();
    console.info("\n\n YL pairs : " + JSON.stringify(XL));

    // Step 6: Determine places p(A, B)from pairs (A, B)

    // Step 7: Construct flow (nodes, edges)
    const [nodes, edges] = alphaPlusAlgo.getFlowRelation(XL, TL, oneLoopTransitions, firstTrans, lastTrans);
    {
        console.log("\n\n Nodes & Edges :");
        nodes.forEach((node, index) => {
            console.info(index + " - " + node.group + " " + node.label);
        });

        edges.forEach((edge, index) => {
            console.info(index + " - Edge from " + edge.from + " to " + edge.to);
        });
    }

    // Step 8: Retrieve XOR/AND-split
    [XOR_Split, XOR_Join] = enhancement.retrieveDecisionPts(nodes, edges);

    return [{ transitions: TL, footprint: footprint }, nodes, edges, XOR_Split, XOR_Join];
}

exports.processdiscovery = function (projectName, res) {
    // var projectName = '';
    // req.on('data', function (data) {
    //     projectName += data;
    // })
    // req.on('end', function () {

    console.log("Project name: " + projectName);
    // Get LOG
    var caseActivities = projectManager.loadJSON(projectName, '/casesActivities.json');
    var Log = [];
    for (var nextcase in caseActivities) {
        Log.push(caseActivities[nextcase]);
    }
    var [footprint, nodes, edges, XOR_Split, XOR_Join] = useAlpha(Log);
    projectManager.saveWorkflowNet(projectName, footprint, {
        'nodes': nodes,
        'edges': edges,
        'XOR_Split': XOR_Split,
        'XOR_Join': XOR_Join
    });

    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify({ data: 'OK' }));
    // })
}

exports.newfunct = function (req) {
    var projectName = '';

    req.on('data', function (data) {
        projectName += data;
    });

    req.on('end', function () {

    });
}