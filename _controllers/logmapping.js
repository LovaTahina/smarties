const csv2json = require('../lib/csv2json');
const fieldPositions = require('../lib/events');
const dataParser = require('../lib/parseEventData');
const projectManager = require('../lib/projectManager');


exports.getCasesActivities = function (req, res) {
    dataParser.readEventLog(req, res, projectManager.createProject(), dataParser.OPERATION.CASES_ACTIVITIES);
}

exports.replay = function (req, res, projectName) {
    dataParser.readEventLog(req, res, projectName, dataParser.OPERATION.REPLAY);
}

exports.changePos = function (req, res) {
    var remaining = '';
    req.on('data', function (data) {
        remaining += data;
    });

    req.on('end', function () {
        console.log(remaining);
        var neededPositions = JSON.parse(remaining);

        if (neededPositions.hasOwnProperty('projectId')) {

            dataParser.getCasesActivities(neededPositions.projectId,
                projectManager.getDataPathForProject(neededPositions.projectId),
                neededPositions,
                res
                );
        }
    });
}