const projectManager = require('../lib/projectManager');
const Model = require('../machinelearning/model');
const learning = require('../machinelearning/learning');
const logistic = require('../machinelearning/logisticRegression');
const linear_algebra = require('../machinelearning/algebra/linear-algebra.min.js')(),
    // initialise it
    Vector = linear_algebra.Vector,
    Matrix = linear_algebra.Matrix;

const nodeFPGrowth = require('node-fpgrowth');

function getInfos(projectId, property, value) {
    var filename = projectManager.encodeFilename(value.trim());
    return projectManager.getDataInfo(projectId, projectManager[property], filename);
}

function compare(a, b) {
    let comp = 0;

    if (a.items.length > b.items.length)
        comp = -1;
    else if (a.items.length < b.items.length)
        comp = 1; //&& a.support > b.support;

    return comp;
}

Array.prototype.onlyUnique = function () {
    var i = this.length;
    while (i--) {
        if (this.indexOf(this[i]) != i) {
            this.splice(i, 1);
        }
    }
};

exports.learnXORSplit = function (res, projectId, nodeId) {
    // try {
    // Get nodeId
    var nodes = projectManager.loadNodes(projectId);
    var nodeFound = nodes.filter(elt => {
        return elt.id == parseInt(nodeId, 10);
    });

    // Get next transitions
    var edges = projectManager.loadEdges(projectId);
    var nodesTo = edges.filter(edge => {
        return edge.from === nodeFound[0].id;
    }).map(function (obj) {
        return obj.to;
    });
    var nodeActivities = nodes.filter(node => {
        return nodesTo.indexOf(node.id) >= 0;
    })

    // Get Attributes per transitions
    var attributesPerTransition = {}
    var stringsPerTransitInstance = {} // Unique string per attribute
    var stringCandidates = [];
    var X_perTransition = [];
    // var Y_transitInstance = {};
    var Y_transitInstance = [];

    for (var i = 0; i < nodeActivities.length; i++) {
        stringsPerTransitInstance[i] = [];
        // Y_transitInstance.push([]);
    }

    var transInstance = 0;
    var i = 0;

    var transactions = []; // for FP-Growth
    var activities = [];
    nodeActivities.map(nodeAct => {


        var Y_temp = new Array(nodeActivities.length).fill(0);
        Y_temp[i] = 1;

        // transactions[i] = [];

        attributesPerTransition[i] = getInfos(projectId, 'activityDir', nodeActivities[i].label.trim())
        for (var t = 0; t < attributesPerTransition[i].length; t++) {
            stringsPerTransitInstance[transInstance] = [];

            var obj = attributesPerTransition[i][t];
            for (var p in obj) {
                if (p !== 'resource') {
                    continue;
                }

                if (typeof obj[p] == 'string') {
                    var candidates = obj[p].split(/,| /);
                    candidates.forEach(val => {
                        stringsPerTransitInstance[transInstance].push(val);
                        stringCandidates.push(val);
                    });
                }
                else if (typeof obj[p] == 'object') {
                    for (var o in obj[p]) {
                        let val = obj[p][o];
                        if (typeof val == 'string') {
                            var candidates = val.split(/,| /);
                            candidates.forEach(v => {
                                stringsPerTransitInstance[transInstance].push(v);
                                stringCandidates.push(v);
                            });
                        }
                        else {
                            stringsPerTransitInstance[transInstance].push('' + val);
                            stringCandidates.push('' + val);
                        }
                    };
                }
                else {
                    stringsPerTransitInstance[transInstance].push('' + obj[p]);
                    stringCandidates.push('' + obj[p]);
                }
            }

            // X,Y
            stringsPerTransitInstance[transInstance].onlyUnique();
            // X_perTransition.push(X_perTransition);
            X_perTransition[transInstance] = [];
            // Y_transitInstance[i][transInstance] = 1;
            Y_transitInstance.push(Y_temp);

            let activity = nodeActivities[i].label.trim();
            let transaction = [activity];
            activities.push(activity);
            transaction = transaction.concat(stringsPerTransitInstance[transInstance]);
            // transactions[i].push(transaction);
            transactions.push(transaction);

            transInstance++;
        }

        i++;
    });

    activities.onlyUnique();

    // Expected solution: https://stackoverflow.com/questions/31413749/node-js-promise-all-and-foreach
    // var transactions = {}; // for FP-Growth

    var fpgrowths = new nodeFPGrowth.FPGrowth(.03);
    // Returns itemsets 'as soon as possible' through events.
    fpgrowths.on('data', (itemset) => {
        // Do something with the frequent itemset.
        let support = itemset.support;
        let items = itemset.items;
    });
    fpgrowths.exec(transactions)
        .then((itemsets) => {
            // // Returns an array representing the frequent itemsets.
            // var itemFiltered = itemsets.filter((obj) => {
            //     return activities.indexOf(obj.items[0]) !== -1
            //         && obj.items.length >= 4;
            // });
            // console.log("\nActivities : " + JSON.stringify(activities));
            // //console.log("\nFP : " + JSON.stringify(itemsets));
            // console.log("\nFP filtered: " + JSON.stringify(itemFiltered));

            var sorted = itemsets.sort(compare);
            var fpSamples = [];

            for (var index = 0; index < nodeActivities.length; index++) {
                var notfound = true;
                for (var i = 0; notfound && i < itemsets.length; i++) {
                    notfound = itemsets[i].items[0].indexOf(nodeActivities[index].label.trim());

                    if (!notfound) {
                        fpSamples.push(itemsets[i]);
                    }
                }
            }
            console.log("\nFP filtered: " + JSON.stringify(fpSamples));
        });
    stringCandidates.onlyUnique();

    // Transform text to int
    var theta_ = [];
    for (var i = 0; i < transInstance; i++) {
        for (var s = 0; s < stringCandidates.length; s++) {
            let val = stringsPerTransitInstance[i].indexOf(stringCandidates[s]);
            let x = val === -1 ? 0 : 1;
            X_perTransition[i].push(x);
        }
    }

    for (var s = 0; s <= stringCandidates.length; s++) {
        theta_.push((Math.random() - 0.5) * 100);
    }

    var X = new Matrix(X_perTransition);
    var Y = new Matrix(Y_transitInstance[0]);
    theta = new Matrix(theta_);

    var iter = 10000;
    var lambda = 1;

    var model = learning.learningLayers(X_perTransition, Y_transitInstance, X_perTransition,
        stringCandidates.length, transInstance, nodeActivities.length, projectId);


    // {
    //     var model = learning.learningRegression(logistic.computeCostGrad, theta, X, Y, lambda, iter, 0.1);

    //     console.info("J : " + JSON.stringify(model._costFunction) + ' --- Expected cost (approx): 0.218')
    //     console.info("Grad : " + JSON.stringify(model._gradient) + ' --- Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n');
    //     console.info('Theta : ' + JSON.stringify(model._params));
    //     console.info('--------------------\n');

    //     console.log("X.dot(theta) - Y = " + JSON.stringify((X.dot(theta.trans())).sigmoid().minus(Y.trans())));
    // }
    // }
    // catch (err) {
    //     console.log("ERROR: while trying to learn XOR Split. " + err);
    // }

    
}


exports.retrieveAttribs = function (res, projectId, nodeValue) {
    var infos = getInfos(projectId, 'activityDir', nodeValue)

    // To IHSM
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(infos));
}

exports.retrieveDecisionPts = function (nodes, edges) {

    // https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
    // https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/reduce

    var decisionPoints = function (array, property) {
        const groupedMap = array.reduce(
            (entryMap, e) => entryMap.set(e[property], [...entryMap.get(e[property]) || [], e]),
            new Map()
        );

        const froms = new Map(
            [...groupedMap]
                .filter(([k, v]) => v.length > 1)
        );

        return froms;
    }

    var froms = decisionPoints(edges, 'from');
    var tos = decisionPoints(edges, 'to');
    console.log(froms);

    var retrievNodeSplits = function (array, groupType) {
        var arr = new Array(groupType.length);
        for (var j = 0; j < groupType.length; j++) {
            arr[j] = [];
        }
        for (var i = 0; i < array.length; i++) {
            for (var j = 0; j < groupType.length; j++) {
                if (array[i].group === 'place' &&
                    groupType[j] === 'XOR-Split' &&
                    froms.has(array[i].id)) {
                    arr[j].push(array[i])
                }
                else if (array[i].group === 'place' &&
                    groupType[j] === 'XOR-Join' &&
                    tos.has(array[i].id)) {
                    arr[j].push(array[i])
                }
            }
        }
        return arr;
    }

    var [XOR_Split, XOR_Join] = retrievNodeSplits(nodes, ['XOR-Split', 'XOR-Join']);
    console.log("\n XOR split : " + XOR_Split);
    console.log("\n XOR Join : "  + XOR_Join);

    return [XOR_Split, XOR_Join];
}