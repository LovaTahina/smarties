const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
const url = require('url');
const helper = require('./lib/helper').Helper;
const router = express.Router();
const app = express();
const project = require('./lib/projectManager');

///////////////////////////////////////////////////////////////////////////////////////

const logmapping = require('./_controllers/logmapping');
const processdisco = require('./_controllers/processdisco');
const enhancement = require('./_controllers/enhancement');

///////////////////////////////////////////////////////////////////////////////////////

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/_views'));
app.use(session({
    secret: 'ssshhhhh', saveUninitialized: true, resave: true,
    cookie: { secure: true, maxAge: 60000 }
}));

var sess; // global session, NOT recommended

////////////////////////////// GET ///////////////////////////////////////////

router.get('/', (req, res) => {
    console.log("GET /: " + req);
    sess = req.session;
    if (sess.email) {
        return res.redirect('/admin');
    }
    res.sendFile('index.html');
});

app.get('/*.*', function (req, res) {
    var options = url.parse(req.url, true);
    console.log("GET /*.*: " + JSON.stringify(options));

    var mime = helper.getMime(options);
    helper.serveFile(res, __dirname + '/' + options.pathname, mime);
});

router.get('/activePrj', function (req, res) {
    console.log("GET Active Project: " + req);
    res.writeHead(200, { 'Content-Type': helper.types['text'] });
    res.end(sess.projectId);
});

router.get('/projectDetails', (req, res) => {
    console.log("GET Project details : " + req);
    const reqUrl = url.parse(req.url, true);
    console.log(reqUrl.query);
    project.getProjectDetails(reqUrl.query.prj, res);
});

router.get('/admin', (req, res) => {
    sess = req.session;
    if (sess.email) {
        res.write(`<h1>Hello ${sess.email} </h1><br>`);
        res.end('<a href=' + '/logout' + '>Logout</a>');
    }
    else {
        res.write('<h1>Please login first.</h1>');
        res.end('<a href=' + '/' + '>Login</a>');
    }
});

router.get('/logout', (req, res) => {
    req.session.destroy((err) => {
        if (err) {
            return console.log(err);
        }
        res.redirect('/');
    });

});

////////////////////////////// POST ///////////////////////////////////////////

router.post('/changePrjDetails', (req, res) => {
    console.log("changePrjDetails");
    console.log(req.body.projectId + ' becomes ' + req.body.label + '. Description: ' + req.body.description);
    setSession(req, res);
    req.session.save(function () {
        project.changeProjectProperties(req, res);
    });
});

router.post('/deletePrj', (req, res) => {
    console.log('Deleting ' + req.body.projectId);
    project.deleteProject(req.body.projectId);
    res.end('done');
});

router.post('/uploadLog', (req, res) => {
    setSession(req, res);
    console.log('Uploading event log ' + req.body.projectId);
    logmapping.getCasesActivities(req, res);
});

router.post('/changeAttribPos', (req, res) => {
    setSession(req, res);
    console.log('Changing attributes position ' + req.body.projectId);
    logmapping.changePos(req, res);
});

router.post('/mineProcess', (req, res) => {
    setSession(req, res);
    console.log('Mining processes ' + req.body.projectId);
    processdisco.processdiscovery(req.body.projectId, res);
});

router.post('/attributes.html', (req, res) => {
    setSession(req, res);
    console.log('Mining attributes ');
    const reqUrl = url.parse(req.url, true);
    var query = reqUrl.query;
    enhancement.retrieveAttribs(res, query.projectId, query.activity);
});

router.post('/learn_xor', (req, res) => {
    setSession(req, res);
    const reqUrl = url.parse(req.url, true);
    console.log('Learning XOR Split');
    var query = reqUrl.query;
    enhancement.learnXORSplit(res, query.projectId, query.nodeId);
});

router.post('/replay', (req, res) => {
    const reqUrl = url.parse(req.url, true);
    console.log(reqUrl.query);
    console.log('Replay ' + reqUrl.query.projectId);
    logmapping.replay(req, res, reqUrl.query.projectId);
});

router.post('/login', (req, res) => {
    sess = req.session;
    sess.email = req.body.email;
    res.end('done');
});

app.use('/', router);


app.listen(process.env.PORT || 8080, () => {
    console.log(`App Started on PORT ${process.env.PORT || 8080}`);
});

function setSession(req, res) {
    if (!req.session.projectId) {
        req.session.projectId = req.body.projectId;
    }
    sess = req.session;
}
