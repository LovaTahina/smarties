const util = require('util')

var RELATION = {
    PRECEED: '>',
    CAUSALITY: '->',
    CAUSALITY_INV: '<-',
    PARALLEL: '||',
    UNRELATED: '#',
    TRIANGLE: 'DT',
    DIAMOND: 'DM'
}

const onlyThisRelation = (relation, elt) => {

    return elt.relation === relation;
}

const onlyLR = (lh, rh, elt) => {
    return elt.lh === lh && elt.rh === rh;
}

Array.prototype.onlyUnique = function () {
    var i = this.length;
    while (i--) {
        if (this.indexOf(this[i]) != i) {
            this.splice(i, 1);
        }
    }
};

Array.prototype.onlyUniqueObj = function (predic) {
    // console.log("\n onlyUniqueObj before: " + JSON.stringify(this));
    var i = this.length;
    while (i--) {
        if (this.findIndex(predic.bind(this, this[i])) != i) {
            // console.log("  x slice("+i+", 1)");
            this.splice(i, 1);
        }
    }
    // console.log("\n onlyUniqueObj so far: " + JSON.stringify(this));
};

Array.prototype.getIndexesOf = function (a) {
    var indexes = [], i = -1;
    while ((i = this.indexOf(a, i + 1)) != -1) {
        indexes.push(i);
    }
    return indexes;
};

Array.prototype.getRelation = function (a, b) {
    let aIndexes = this.getIndexesOf(a);
    let bIndexes = this.getIndexesOf(b);

    console.info("aIndexes : " + aIndexes);
    console.info("bIndexes : " + bIndexes);

    let ia = 0, ib = 0;
    let a2b = RELATION.UNRELATED,
        b2a = RELATION.UNRELATED;
    for (va = aIndexes[ia], vb = bIndexes[ib];
        ia < aIndexes.length && ib < bIndexes.length;) {
        if (va === vb - 1) {
            a2b = RELATION.PRECEED;
            ia++;
        }
        else if (vb === va - 1) {
            b2a = RELATION.PRECEED;
            ib++;
        }
        else if (va < vb - 1) {
            ia++;
        }
        else if (vb < va - 1) {
            ib++;
        }
        else {
            ia++;
            ib++;
        }
    }

    // Final conclusion
    let finaly = a2b;
    if (RELATION.PRECEED === a2b && RELATION.PRECEED === b2a) {
        finaly = RELATION.PARALLEL;
    }

    return finaly;
};

Array.prototype.checkUnRelated = function (footprint) {
    console.info("\n\n checkUnRelated : ")
    var unrelated = true;
    for (i = 0; i < this.length; i++) {
        var lh = this[i]
        for (j = i + 1; j < this.length; j++) {
            var rh = this[j];
            var elt = footprint.filter(onlyLR.bind(this, lh, rh));
            console.info(lh + " vs " + rh + " -> " + JSON.stringify(elt));
            if (elt[0].relation !== RELATION.UNRELATED) {
                unrelated = false;
                break;
            }
        }
    }
    return unrelated;
}

Array.prototype.removeNonMaxPairs = function () {

    this.sort((a, b) => (b.rh.length > a.rh.length) ? 1 : ((a.rh.length > b.rh.length) ? -1 : 0));

    for (i = this.length - 1; i >= 0; i--) {
        var objR = this[i];
        for (j = i - 1; j >= 0; j--) {
            var objL = this[j];

            if (arraysEqual(objR.lh, objL.lh)) {
                var hasSub = objL.rh.hasSubArray(objR.rh);
                var msg = '(' + JSON.stringify(objL) + ' & ' + JSON.stringify(objR) + ') ' +
                    JSON.stringify(objL.rh) + ' VS ' + JSON.stringify(objR.rh) + " => " + hasSub;
                if (hasSub) {
                    msg += ' so ' + JSON.stringify(objR) + ' is removed';
                }
                console.info(msg);
                if (hasSub) {
                    this.splice(i, 1);
                    break;
                }
            }
        }
    }

    this.sort((a, b) => (b.lh.length > a.lh.length) ? 1 : ((a.lh.length > b.lh.length) ? -1 : 0));

    for (i = this.length - 1; i >= 0; i--) {
        var objR = this[i];
        for (j = i - 1; j >= 0; j--) {
            var objL = this[j];
            if (arraysEqual(objR.rh, objL.rh)) {
                var hasSub = objL.lh.hasSubArray(objR.lh);
                var msg = '(' + JSON.stringify(objL) + ' & ' + JSON.stringify(objR) + ') ' +
                    JSON.stringify(objL.lh) + ' VS ' + JSON.stringify(objR.lh) + " => " + hasSub;
                if (hasSub) {
                    msg += ' so ' + JSON.stringify(objR) + ' is removed';
                }
                console.info(msg);
                if (hasSub) {
                    this.splice(i, 1);
                    break;
                }
            }
        }
    }
}

Array.prototype.removeRedundant = function (footprint) {
    var i = this.length;
    while (i--) {
        let index = this.findIndex(oneLoop => oneLoop.transition === this[i].transition);
        if (index !== i) {
            this[i].from.forEach(from => {
                if (!this[index].from.includes(from)) {
                    this[index].from.push(from);
                }
            });
            this[i].to.forEach(to => {
                if (!this[index].to.includes(to)) {
                    this[index].to.push(to);
                }
            });
            this.splice(i, 1);
        }
    }
}

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

Array.prototype.hasSubArray = function (sub) {
    return sub.every((i => v => i = this.indexOf(v, i) + 1)(0));
}

// For Array of array [[a,b], ...]
Array.prototype.containsPair = function (a, b) {
    let i = 0;
    let found = false;
    while (!found && i < this.length) {
        // console.log("a:" + a + ", this[i][0]:" + this[i][0] + ", b:" + b + ", this[i][1]:" + this[i][1]);
        if (a == this[i][0] && b == this[i][1]) {
            found = true;
        }
        i++;
    }
    return found;
}

exports.composeElt = function (arr, footprint) {
    var subs = arr.reduce(
        (subsets, value) => subsets.concat(
            subsets.map(set => [value, ...set])
        ),
        [[]]
    );

    var composed = [];
    subs.forEach(elt => {
        if (elt.checkUnRelated(footprint)) {
            composed.push(elt);
        }
    });

    return composed;
}
exports.getUniqueNSeqTransitions = function (Log) {

    var triangles = [];
    var diamonds = [];

    // alpha+ Pre-processing
    var oneLoopTransitions = [];

    for (r = 0; r < Log.length; r++) {
        var c = Log[r].length;
        while (c--) {

            if (c > 1 && c < Log[r].length - 1) {

                // catch attb (one-loop tt)
                let a = Log[r][c + 1];
                let t1 = Log[r][c];
                let b = null;
                var nb = 0;
                var cOld = c;
                var found = true;
                while (c > 1 && found) {
                    found = false;
                    let t2 = Log[r][c - 1];
                    if (t1 === t2) {
                        b = Log[r][c - 2];
                        c--;
                        nb++;
                        found = true;
                    }
                }
                if (nb > 0) {
                    oneLoopTransitions.push({ transition: t1, from: [b], to: [a] }); // assume a > t and t > b
                    Log[r].splice(cOld - nb, nb + 1); // remove ...tt....
                }
            }
        }
    }

    oneLoopTransitions.onlyUniqueObj(this.checkSameObjects);

    // Remove remaining 0 or 1 occurrence of oneLoopTransitions ...t...
    var transitions = [];
    for (r = 0; r < Log.length; r++) {
        var c = Log[r].length;
        while (c--) {

            var l = oneLoopTransitions.length;
            var notfound = true;
            while (notfound && l--) {
                if (oneLoopTransitions[l].transition === Log[r][c]) {
                    notfound = false;
                    Log[r].splice(c, 1);
                }
            }
            if (Log[r][c]) {
                transitions.push(Log[r][c]);

                if (c > 1) {
                    // catch aba or bab  (2-loop)
                    let a1 = Log[r][c];
                    let b_ = Log[r][c - 1];
                    let a2 = Log[r][c - 2];
                    if (a1 === a2 && a1 !== b_) {
                        triangles.push([a1, b_]);
                    }
                }
            }
        }
    }

    transitions.onlyUnique();
    triangles.onlyUniqueObj(this.checkSameObjects);

    // Catch diamond from triangles
    for (var i = 0; i < triangles.length; i++) {
        let j = i + 1;
        var found = false;
        while (!found && j < triangles.length) {
            found = (triangles[i][1] == triangles[j][0] && triangles[i][0] == triangles[j][1]);
            if (found) {
                diamonds.push([triangles[j][0], triangles[j][1]]);
            }
            j++;
        }
    }

    return [transitions, triangles, diamonds, oneLoopTransitions];
}

exports.checkSameObjects = function (a, b) {
    var same = true;
    //console.log("\n\n checkSameObjects(" + JSON.stringify(a) + ", " + JSON.stringify(b) + ")");
    for (var attrb in a) {
        same &= a[attrb] === b[attrb];
        //console.log("attrb :" + attrb + "; a[attrb]=" + a[attrb] + "; b[attrb]=" + b[attrb] + " => " + same);
    }
    return same;
}

exports.checkSameRelation = function (a, b) {
    return (a.lh === b.lh &&
        a.relation === b.relation &&
        a.rh === b.rh);
}

exports.getDirectSuccessions = function (Log) {
    var directSuccessions = [];
    Log.forEach(logInstance => {
        console.log("Instance : " + logInstance);
        for (lh = 0; lh < logInstance.length - 1; lh++) {
            directSuccessions.push({
                lh: logInstance[lh],
                relation: RELATION.PRECEED,
                rh: logInstance[lh + 1]
            });
        }
    });

    directSuccessions.onlyUniqueObj(this.checkSameRelation);
    return directSuccessions;
}

exports.getTransitionsFootPrint = function (Log) {

    // 1. Retrieve unique transitions, triangles and diamond relations
    var [transitions, triangles, diamonds, oneLoopTransitions] = this.getUniqueNSeqTransitions(Log);
    console.log("\n oneLoopTransitions : " + JSON.stringify(oneLoopTransitions));

    // 2. Direct successions
    var directSuccessions = this.getDirectSuccessions(Log);
    console.info("\n\ndirectSuccessions : " + JSON.stringify(directSuccessions));

    // 3. Ordering relation helper function
    directSuccessions.getOrderingRelation = function (a, b) {

        console.log("\n\n _________ Get Ordering relation (" + a + ", " + b + ") ________");
        var i = 0;
        var a2bRelation = RELATION.UNRELATED;
        var diamondRelation = diamonds.containsPair(a, b) || diamonds.containsPair(b, a);
        var tri_ab = triangles.containsPair(a, b);
        var tri_ba = triangles.containsPair(b, a);

        console.log("\t tri_ab : " + tri_ab);
        console.log("\t tri_ba : " + tri_ba);
        console.log("\t diamond : " + diamondRelation);

        var setRelation = (txt, a, b, relation) => {
            let before = a2bRelation;
            a2bRelation = relation;
            console.log("\t\t | " + txt + " : " + a + before + b + " become " + a + a2bRelation + b);
        }

        var a_precede_b = false;
        var b_precede_a = false;
        var a_parallele_b = false;

        while (i < this.length) {

            // Potentialy Direct succession RELATION.PRECEED
            if (a === this[i].lh && b === this[i].rh) {
                a_precede_b = true;
            }

            if (b === this[i].lh && a === this[i].rh) {
                b_precede_a = true;
            }

            i++;
        }

        if (a_precede_b && (!b_precede_a || diamondRelation)) {
            setRelation("(1) " + a + ">" + b + " && (!" + b + ">" + a + " || diam(" + a + "," + b + ") is true? "
                + diamondRelation, a, b, RELATION.CAUSALITY);
        }
        else if (b_precede_a && (!a_precede_b || diamondRelation)) {
            setRelation("(1 inv) " + a + "<" + b + " && (!" + b + "<" + a + " || diam(" + a + "," + b + ") is true? "
                + diamondRelation, a, b, RELATION.CAUSALITY_INV);
        }
        else if (a_precede_b && b_precede_a && !diamondRelation) {
            setRelation("(2) " + a + ">" + b + " && " + b + ">" + a + " && diam(" + a + "," + b + ") is false? "
                + diamondRelation, a, b, RELATION.PARALLEL);
        }
        else {
            setRelation("(3) not found", a, b, RELATION.UNRELATED);
        }

        return a2bRelation;
    }

    // 4. Get ordering relations
    var footprint = [];
    for (lh = 0; lh < transitions.length; lh++) {
        for (rh = 0; rh < transitions.length; rh++) {

            var rel = directSuccessions.getOrderingRelation(transitions[lh], transitions[rh]);
            footprint.push({
                lh: transitions[lh],
                relation: rel,
                rh: transitions[rh]
            });
        }
    }

    // 5. Sort and filter data
    transitions.sort();
    footprint.onlyUniqueObj((fpL, fpR) => {
        return fpL.lh === fpR.lh &&
            fpL.relation === fpR.relation &&
            fpL.rh === fpR.rh;
    })
    footprint.sort((fpL, fpR) => {
        return (fpL.lh > fpR.lh || (fpL.lh === fpR.lh && fpL.rh > fpR.rh)) ? 1 : -1;
    });

    // 6. Merge oneLoopTransitions
    oneLoopTransitions.removeRedundant(footprint);

    return [transitions, footprint, oneLoopTransitions];
}

exports.getFirstLastTransitions = function (footprint) {

    var directCausal = footprint.filter(onlyThisRelation.bind(this, RELATION.CAUSALITY));
    var parallel = footprint.filter(onlyThisRelation.bind(this, RELATION.PARALLEL));
    // var inverseCausal = footprint.filter(onlyCausal.bind(this,
    //     processmining.RELATION.CAUSALITY_INV));

    console.log("----------------getFirstLastTransitions from " + directCausal + " -------------------");

    var first = [];
    for (i = 0; i < directCausal.length; i++) {
        let isNotOnRight = true;
        let lh = directCausal[i].lh;
        for (j = 0; j < directCausal.length; j++) {
            isNotOnRight &= lh !== directCausal[j].rh;
            console.log("\t\t lh[" + i + "] : " + lh + ", rh[" + j + "] : " + directCausal[j].rh + " => " + isNotOnRight);
        }
        let isNotInParallel = true;
        for (j = 0; j < parallel.length; j++) {
            isNotInParallel &= parallel[j].lh !== lh && parallel[j].rh !== lh
        }
        if (isNotOnRight && isNotInParallel) {
            first.push(lh);
        }
    }
    first.onlyUnique();

    var last = [];
    for (i = 0; i < directCausal.length; i++) {
        let isNotOnRight = true;
        let rh = directCausal[i].rh;
        for (j = 0; j < directCausal.length; j++) {
            isNotOnRight &= rh !== directCausal[j].lh;
            console.log("\t\t rh[" + i + "] : " + rh + ", lh[" + j + "] : " + directCausal[j].lh + " => " + isNotOnRight);
        }
        let isNotInParallel = true;
        for (j = 0; j < parallel.length; j++) {
            isNotInParallel &= parallel[j].lh !== rh && parallel[j].rh !== rh
        }
        if (isNotOnRight && isNotInParallel) {
            last.push(rh);
        }
    }
    last.onlyUnique();

    console.log("----------------first : " + first + "; last : " + last + " ------------------");

    return [first, last];
}

exports.findPairsOfSets = function (footprint, firsTrans, lastTrans) {

    var uniquePairsLeft = new Map();
    var uniquePairsRight = new Map();

    Map.prototype.hasKey = function (key) {

        console.log("\n | key: " + key);
        if (typeof key === 'string') {
            console.log(" | key is string: is found? " + this.has(key));
            return this.has(key);
        }

        console.log(" | key is not string");
        var keyFound = false;
        for (var entry of this.entries()) {

            let key_ = entry[0];
            let value_ = entry[1];
            console.log(" | map[" + key_ + "] = " + value_);
            keyFound = arraysEqual(key, key_);
            console.log(" | key is found? " + keyFound);

            if (keyFound) {
                break;
            }
        }
        return keyFound;
    }

    Map.prototype.getValue = function (key) {
        console.log("\n | key: " + key);
        if (typeof key === 'string') {
            console.log(" | key is string");
            return this.get(key);
        }
        else {
            for (var entry of this.entries()) {

                let key_ = entry[0];
                let value_ = entry[1];
                console.log(" | map[" + key_ + "] = " + value_);
                let keyFound = arraysEqual(key, key_);
                console.log(" | key is found? " + keyFound);

                if (keyFound) {
                    return entry[1];
                }
            }
        }
        return null;
    }

    /////////// a -> b ///////////////////////////////////////////////////////

    var addUpdatePair = (pairMap, key, value) => {
        if (!pairMap.hasKey(key)) {
            var arr = [value];
            pairMap.set(key, arr);
            console.log("Key not found " + key + ' => ' + util.inspect(pairMap));
        }
        else {
            var arr = pairMap.getValue(key);
            if (typeof value === 'string') {
                arr.push(value);
                console.log("(key:" + key + ', value: ' + value + ') => ' + util.inspect(pairMap));
            }
            else {
                var arrConcat = arr.concat(value);
                pairMap.set(key, arrConcat);
                console.log("(key:" + key + ', value: ' + value + ') => ' + util.inspect(paiMap));
            }
        }
    }

    var pushToX = (lh_, rh_) => {
        addUpdatePair(uniquePairsLeft, lh_, rh_);
        //addUpdatePair(uniquePairsRight, rh_, lh_);
    }

    console.log("\n\n Causaly related: ")
    var onlyCausality = footprint.filter(onlyThisRelation.bind(this, RELATION.CAUSALITY));
    for (i = 0; i < onlyCausality.length; i++) {
        console.log("" + i + " " + onlyCausality[i].lh + " " + onlyCausality[i].relation + " " + onlyCausality[i].rh);
        pushToX(onlyCausality[i].lh, onlyCausality[i].rh);
    }

    console.log("\n uniquePairsLeft : " + util.inspect(uniquePairsLeft));

    uniquePairsLeft.forEach((val, key, map) => {
        addUpdatePair(uniquePairsRight, val, key);
    });


    /////////// a # b only ///////////////////////////////////////////////////////

    // var XL = [];
    // uniquePairsLeft.forEach((val, key, map) => {
    //     var composed = val.composeElt(footprint);
    //     composed.forEach(val_ => {
    //         XL.push({
    //             lh: [key],
    //             rh: val_
    //         });
    //     });
    // });

    // TODO
    /////////// a || b and a, b not existing ///////////////////////////////////////////////////////
    console.log("\n\n Parallel related: ")
    var onlyParallel = footprint.filter(onlyThisRelation.bind(this, RELATION.PARALLEL));
    for (i = 0; i < onlyParallel.length; i++) {

        // Fix left
        let leftA = uniquePairsLeft.getValue(onlyParallel[i].lh);
        let leftB = uniquePairsLeft.getValue(onlyParallel[i].rh);

        if(   (leftA === undefined)
           || (leftB === undefined)
           //|| (leftA !== leftB)
           ){
            console.log("---------- Fix missing AND-Split --------");
            console.log("" + i + " " + onlyParallel[i].lh + " " + onlyParallel[i].relation + " " + onlyParallel[i].rh);
            pushToX(onlyParallel[i].lh, onlyParallel[i].rh);
        }

        // Fix right
        let leftC = uniquePairsRight.getValue(onlyParallel[i].lh);
        let leftD = uniquePairsRight.getValue(onlyParallel[i].rh);

        if(   (leftC === undefined)
           || (leftD === undefined)
           //|| (leftA !== leftB)
           ){
            console.log("---------- Fix missing AND-join --------");
            console.log("" + i + " " + onlyParallel[i].lh + " " + onlyParallel[i].relation + " " + onlyParallel[i].rh);
            pushToX(onlyParallel[i].lh, onlyParallel[i].rh);
        }

    }

    console.log("\n uniquePairsLeft : " + util.inspect(uniquePairsLeft));

    uniquePairsLeft.forEach((val, key, map) => {
        addUpdatePair(uniquePairsRight, val, key);
    });

    console.log('\n ___________________ uniquePairsRight______________\n' + util.inspect(uniquePairsRight));
    var XL0 = [];
    uniquePairsRight.forEach((val, key, map) => {
        var composed = this.composeElt(val, footprint);
        composed.forEach(val_ => {
            XL0.push({
                lh: val_,
                rh: key
            });
        });
    });

    console.log('\n ___________________ XL0 ___________________\n' + util.inspect(XL0));

    var XL = [];
    XL0.forEach(elt => {
        var composed = this.composeElt(elt.rh, footprint);
        composed.forEach(rh_ => {
            XL.push({
                lh: elt.lh,
                rh: rh_
            });
        });
    });

    console.log("\n____________________ XL _______________________\n " + JSON.stringify(XL));

    return XL;
}

exports.getFlowRelation = function (YL, TL, oneLoopTransitions, firstTrans, lastTrans) {

    var xI = 10, xO = 1000;
    var indexNode = -1;
    var nodes = [];
    var edges = [];

    //console.log("\n\n Nodes & Edges");

    // 1. Record transitions into Node
    var transitionMap = new Map;
    // var span = 20;
    // var yIO = 250 - ((TL.length-1) * span/2)
    for (i = 0; i < TL.length; i++) {
        indexNode++;
        var node = { 'id': indexNode, label: TL[i], group: "transition" };
        // TODO: Apply to firstTrans and lastTrans but not here
        // if (i === 0) {
        //     node['x'] = xI + span;
        //     node['y'] = yIO + i*span;
        // }
        // else if (i === TL.length-1) {
        //     node['x'] = xO - span;
        //     node['y'] = yIO + i*span;
        // }
        nodes.push(node);
        //console.info(indexNode + " - transition " + TL[i]);
        transitionMap.set(TL[i], indexNode);
    };

    // 2. Add Input Place to Nodes
    indexNode++;
    nodes.push({ 'id': indexNode, label: 'I', group: "place" });
    //console.info(indexNode + " - Place I ");

    // 3. Add Edge between Input Place and Input Transitions
    var indexEdge = -1;
    for (i = 0; i < firstTrans.length; i++) {
        indexEdge++;
        let nodeTo = transitionMap.get(firstTrans[i]);
        edges.push({
            id: indexEdge,
            from: indexNode,
            to: nodeTo,
            'arrows': 'to',
            'physics': false,
            'smooth': {
                'type': 'dynamic' /*'cubicBezier'*/
            }
        });
        //console.info(indexEdge + " - Edge from " + indexNode + " to " + nodeTo);
    }

    // 4. Add other Places to Nodes and
    //              Edges "from" between transitions and place,
    //              Edges "to"   between places and transition
    var placeMap = new Map;
    var p = 0;
    var q = 0;
    for (i = 0; i < YL.length; i++) {

        // 4.1 Add place
        indexNode++;
        nodes.push({ 'id': indexNode, label: 'p' + indexNode, group: "place" });

        placeMap.set(TL[i], indexNode);
        for (p = 0; p < YL[i].lh.length; p++) {
            for (q = 0; q < YL[i].rh.length; q++) {

                // 4.2. Add Edge "From"
                let nodeFrom = transitionMap.get(YL[i].lh[p]);
                indexEdge++;
                edges.push({
                    id: indexEdge,
                    from: nodeFrom,
                    to: indexNode,
                    'arrows': 'to',
                    'physics': false,
                    'smooth': {
                        'type': 'dynamic' /*'cubicBezier'*/
                    }
                });
                //console.info(indexEdge + " - Edge from " + nodeFrom + " to " + indexNode);

                // 4.2. Add Edge "To"
                let nodeTo = transitionMap.get(YL[i].rh[q]);
                indexEdge++;
                edges.push({
                    id: indexEdge,
                    from: indexNode,
                    to: nodeTo,
                    'arrows': 'to',
                    'physics': false,
                    'smooth': {
                        'type': 'dynamic' /*'cubicBezier'*/
                    }
                });
                //console.info(indexEdge + " - Edge from " + indexNode + " to " + nodeTo);
            }
        }
    };

    // 5. Add Output Place to Nodes
    indexNode++;
    nodes.push({ 'id': indexNode, label: 'O', group: "place" });

    // 6. Add Edge between Ouput Transitions and Input Place
    for (i = 0; i < lastTrans.length; i++) {
        let nodeFrom = transitionMap.get(lastTrans[i]);
        indexEdge++;
        edges.push({
            id: indexEdge,
            from: nodeFrom,
            to: indexNode,
            'arrows': 'to',
            'physics': false,
            'smooth': {
                'type': 'dynamic' /*'cubicBezier'*/
            }
        });
        //console.info(indexEdge + " - Edge from " + nodeFrom + " to " + indexNode);
    }

    // Additional step for alpha+: post-processing
    oneLoopTransitions.forEach(oneLoop => {

        let fromTransIndex = [],
            toTransIndex = [];
        for (i = 0; i < nodes.length; i++) {
            if (oneLoop.from.includes(nodes[i].label)) {
                fromTransIndex.push(i);
            }
            if (oneLoop.to.includes(nodes[i].label)) {
                toTransIndex.push(i);
            }
        };

        var edgesAdded = false;
        for (var f = 0; f < fromTransIndex.length; f++) {
            for (var t = 0; t < toTransIndex.length; t++) {

                let edgeFromIndexes = edges.filter(edge => edge.from === nodes[fromTransIndex[f]].id);
                let edgeToIndexes = edges.filter(edge => edge.to === nodes[toTransIndex[t]].id);
                var edgeFrom = edgeFromIndexes.filter(function (v) {
                    let notfound = true;
                    let i = edgeToIndexes.length;
                    while (notfound && i--) {
                        notfound &= (v.to !== edgeToIndexes[i].from);
                    }
                    return !notfound;
                });

                if (edgeFrom && edgeFrom.length) {

                    // Postpone node creation
                    if (!edgesAdded) {
                        indexNode++;
                        edgesAdded = true;
                        //nodes.push({ 'id': indexNode, label: oneLoop.transition, group: "transition" });
                    }

                    indexEdge++;
                    edges.push({
                        id: indexEdge,
                        from: edgeFrom[0].to,
                        to: indexNode,
                        'arrows': 'to',
                        'physics': false,
                        'smooth': {
                            'type': 'dynamic' /*'cubicBezier'*/
                        }
                    });

                    indexEdge++;
                    edges.push({
                        id: indexEdge,
                        from: indexNode,
                        to: edgeFrom[0].to,
                        'arrows': 'to',
                        'physics': false,
                        'smooth': {
                            'type': 'dynamic' /*'cubicBezier'*/
                        }
                    });
                }
            }
        }

        if (edgesAdded) {
            nodes.push({ 'id': indexNode, label: oneLoop.transition, group: "transition" });
        }

    });

    // Remove duplicates
    nodes.onlyUniqueObj((nodeL, nodeR) => {
        return nodeL.label === nodeR.label &&
            nodeL.group === nodeR.group;
    });
    edges.onlyUniqueObj((edgeL, edgeR) => {
        return edgeL.from === edgeR.from &&
            edgeL.to === edgeR.to;
    })


    return [nodes, edges];
}

exports.RELATION = RELATION;
