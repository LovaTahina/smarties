const util = require('util')

var RELATION = {
    PRECEED: '>',
    CAUSALITY: '->',
    CAUSALITY_INV: '<-',
    PARALLEL: '||',
    UNRELATED: '#',
}

const onlyThisRelation = (relation, elt) => {

    return elt.relation === relation;
}

const onlyLR = (lh, rh, elt) => {
    return elt.lh === lh && elt.rh === rh;
}

Array.prototype.onlyUnique = function () {
    var i = this.length;
    while (i--) {
        if (this.indexOf(this[i]) != i) {
            this.splice(i, 1);
        }
    }
};

Array.prototype.onlyUniqueObj = function (predic) {
    var i = this.length;
    while (i--) {
        if (this.findIndex(predic.bind(this, this[i])) != i) {
            this.splice(i, 1);
        }
    }
};

Array.prototype.getIndexesOf = function (a) {
    var indexes = [], i = -1;
    while ((i = this.indexOf(a, i + 1)) != -1) {
        indexes.push(i);
    }
    return indexes;
};

Array.prototype.getRelation = function (a, b) {
    let aIndexes = this.getIndexesOf(a);
    let bIndexes = this.getIndexesOf(b);

    console.info("aIndexes : " + aIndexes);
    console.info("bIndexes : " + bIndexes);

    let ia = 0, ib = 0;
    let a2b = RELATION.UNRELATED,
        b2a = RELATION.UNRELATED;
    for (va = aIndexes[ia], vb = bIndexes[ib];
        ia < aIndexes.length && ib < bIndexes.length;) {
        if (va === vb - 1) {
            a2b = RELATION.PRECEED;
            ia++;
        }
        else if (vb === va - 1) {
            b2a = RELATION.PRECEED;
            ib++;
        }
        else if (va < vb - 1) {
            ia++;
        }
        else if (vb < va - 1) {
            ib++;
        }
        else {
            ia++;
            ib++;
        }
    }

    // Final conclusion
    let finaly = a2b;
    if (RELATION.PRECEED === a2b && RELATION.PRECEED === b2a) {
        finaly = RELATION.PARALLEL;
    }

    return finaly;
};

Array.prototype.checkUnRelated = function (footprint) {
    console.info("\n\n checkUnRelated : ")
    var unrelated = true;
    for (i = 0; i < this.length; i++) {
        var lh = this[i]
        for (j = i + 1; j < this.length; j++) {
            var rh = this[j];
            var elt = footprint.filter(onlyLR.bind(this, lh, rh));
            console.info(lh + " vs " + rh + " -> " + JSON.stringify(elt));
            if (elt[0].relation !== RELATION.UNRELATED) {
                unrelated = false;
                break;
            }
        }
    }
    return unrelated;
}

Array.prototype.removeNonMaxPairs = function () {

    this.sort((a, b) => (b.rh.length > a.rh.length) ? 1 : ((a.rh.length > b.rh.length) ? -1 : 0));

    for (i = this.length - 1; i >= 0; i--) {
        var objR = this[i];
        for (j = i - 1; j >= 0; j--) {
            var objL = this[j];

            if (arraysEqual(objR.lh, objL.lh)) {
                var hasSub = objL.rh.hasSubArray(objR.rh);
                var msg = '(' + JSON.stringify(objL) + ' & ' + JSON.stringify(objR) + ') ' +
                    JSON.stringify(objL.rh) + ' VS ' + JSON.stringify(objR.rh) + " => " + hasSub;
                if (hasSub) {
                    msg += ' so ' + JSON.stringify(objR) + ' is removed';
                }
                console.info(msg);
                if (hasSub) {
                    this.splice(i, 1);
                    break;
                }
            }
        }
    }

    this.sort((a, b) => (b.lh.length > a.lh.length) ? 1 : ((a.lh.length > b.lh.length) ? -1 : 0));

    for (i = this.length - 1; i >= 0; i--) {
        var objR = this[i];
        for (j = i - 1; j >= 0; j--) {
            var objL = this[j];
            if (arraysEqual(objR.rh, objL.rh)) {
                var hasSub = objL.lh.hasSubArray(objR.lh);
                var msg = '(' + JSON.stringify(objL) + ' & ' + JSON.stringify(objR) + ') ' +
                    JSON.stringify(objL.lh) + ' VS ' + JSON.stringify(objR.lh) + " => " + hasSub;
                if (hasSub) {
                    msg += ' so ' + JSON.stringify(objR) + ' is removed';
                }
                console.info(msg);
                if (hasSub) {
                    this.splice(i, 1);
                    break;
                }
            }
        }
    }
}

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

Array.prototype.hasSubArray = function (sub) {
    return sub.every((i => v => i = this.indexOf(v, i) + 1)(0));
}

exports.composeElt = function (arr, footprint) {
    var subs = arr.reduce(
        (subsets, value) => subsets.concat(
            subsets.map(set => [value, ...set])
        ),
        [[]]
    );

    var composed = [];
    subs.forEach(elt => {
        if (elt.checkUnRelated(footprint)) {
            composed.push(elt);
        }
    });

    return composed;
}

/*Array.prototype.composeElt = function (footprint) {
    var composedResult = this;
    var nInitial = composedResult.length;
    var nActual = 0;
    var found = false;
    console.log("\n\n this" + JSON.stringify(this));
    if (nInitial !== nActual) {
        nInitial = composedResult.length;
        var composed = [];
        for (var i = 0; i < composedResult.length; i++) {
            var lh = [];
            if ((typeof composedResult[i]) == 'string') {
                lh = [composedResult[i]];
            }
            else {
                lh = [...composedResult[i]];
            }

            for (var j = i + 1; j < composedResult.length; j++) {
                var elts = [];
                if ((typeof composedResult[j]) == 'string') {
                    elts = [...lh, composedResult[j]];
                }
                else {
                    elts = [...lh, ...composedResult[j]];
                }
                console.log("\n  --> lh:" + JSON.stringify(lh) + ", elts" + JSON.stringify(elts))
                elts.onlyUniqueObj(arraysEqual);
                if (elts.checkUnRelated(footprint)) {
                    composed.push(elts);
                    found = true;
                }
                else {
                    if ((typeof composedResult[j]) == 'string') {
                        composed.push([composedResult[j]]);
                    }
                    else {
                        composed.push([...composedResult[j]]);
                    }
                    if ((typeof lh) == 'string') {
                        composed.push([lh]);
                    }
                    else {
                        composed.push([...composedResult[j]]);
                    }
                }
            }
            if (i === composedResult.length - 1) {
                composed.push(lh);
            }
        }
        composedResult = composed;
        nActual = composedResult.length;
    }

    if (!found) {
        composedResult = this.map(elt => {
            return [elt];
        });
    }

    console.log("this : " + JSON.stringify(this) + " => composedResult : " + JSON.stringify(composedResult));
    return composedResult;
}*/

exports.getUniqueTransitions = function (Log) {
    var transitions = [];
    for (r = 0; r < Log.length; r++) {
        for (c = 0; c < Log[r].length; c++) {
            transitions.push(Log[r][c]);
        }
    }

    transitions.onlyUnique();
    return transitions;
}

exports.checkSameRelation = function (a, b) {
    return (a.lh === b.lh &&
        a.relation === b.relation &&
        a.rh === b.rh);
}

exports.getDirectSuccessions = function (Log) {
    var directSuccessions = [];
    Log.forEach(logInstance => {
        console.log("Instance : " + logInstance);
        for (lh = 0; lh < logInstance.length - 1; lh++) {
            directSuccessions.push({
                lh: logInstance[lh],
                relation: RELATION.PRECEED,
                rh: logInstance[lh + 1]
            });
        }
    });

    directSuccessions.onlyUniqueObj(this.checkSameRelation);
    return directSuccessions;
}

exports.getTransitionsFootPrint = function (Log) {

    // 1.Direct successions
    var directSuccessions = this.getDirectSuccessions(Log);
    console.info("\n\ndirectSuccessions : " + JSON.stringify(directSuccessions));

    directSuccessions.getRelation = function (a, b) {
        var i = 0;
        var a2bRelation = RELATION.UNRELATED;
        while (i < this.length) {

            // Potentialy Direct succession RELATION.PRECEED
            if (a === this[i].lh && b === this[i].rh &&
                RELATION.UNRELATED === a2bRelation) {
                a2bRelation = this[i].relation;
            }

            else if (b === this[i].lh && a === this[i].rh &&
                RELATION.PRECEED === a2bRelation &&
                RELATION.PRECEED === this[i].relation) {
                a2bRelation = RELATION.PARALLEL;
            }

            else if (b === this[i].lh && a === this[i].rh &&
                RELATION.PRECEED === a2bRelation &&
                RELATION.UNRELATED === this[i].relation) {
                a2bRelation = RELATION.CAUSALITY;
            }

            else if (b === this[i].lh && a === this[i].rh &&
                RELATION.UNRELATED === a2bRelation &&
                RELATION.PRECEED === this[i].relation) {
                a2bRelation = RELATION.CAUSALITY_INV;
            }

            i++;
        }

        if (RELATION.PRECEED === a2bRelation) {
            a2bRelation = a2bRelation = RELATION.CAUSALITY;
        }

        return a2bRelation;
    }

    // 2. Transitions
    var transitions = this.getUniqueTransitions(Log);

    var footprint = [];
    for (lh = 0; lh < transitions.length; lh++) {
        for (rh = lh; rh < transitions.length; rh++) {

            var rel = directSuccessions.getRelation(transitions[lh], transitions[rh]);
            footprint.push({
                lh: transitions[lh],
                relation: rel,
                rh: transitions[rh]
            });

            var rel_inv = RELATION.UNRELATED;
            if (RELATION.CAUSALITY === rel) {
                rel_inv = RELATION.CAUSALITY_INV
            }
            else if (RELATION.CAUSALITY_INV === rel) {
                rel_inv = RELATION.CAUSALITY
            }
            else if (RELATION.PARALLEL === rel) {
                rel_inv = RELATION.PARALLEL
            }
            footprint.push({
                lh: transitions[rh],
                relation: rel_inv,
                rh: transitions[lh]
            });
        }
    }

    // Sort and filter data
    transitions.sort();
    footprint.onlyUniqueObj((fpL, fpR) => {
        return fpL.lh === fpR.lh &&
            fpL.relation === fpR.relation &&
            fpL.rh === fpR.rh;
    })
    footprint.sort((fpL, fpR) => {
        return (fpL.lh > fpR.lh || (fpL.lh === fpR.lh && fpL.rh > fpR.rh)) ? 1 : -1;
    });

    return [transitions, footprint];
}

exports.getFirstLastTransitions = function (footprint) {

    var directCausal = footprint.filter(onlyThisRelation.bind(this, RELATION.CAUSALITY));
    // var inverseCausal = footprint.filter(onlyCausal.bind(this,
    //     processmining.RELATION.CAUSALITY_INV));

    var first = [];
    for (i = 0; i < directCausal.length; i++) {
        let isNotOnRight = true;
        let lh = directCausal[i].lh;
        for (j = 0; j < directCausal.length; j++) {
            isNotOnRight &= lh !== directCausal[j].rh;
        }
        if (isNotOnRight) {
            first.push(lh);
        }
    }
    first.onlyUnique();

    var last = [];
    for (i = 0; i < directCausal.length; i++) {
        let isNotOnRight = true;
        let rh = directCausal[i].rh;
        for (j = 0; j < directCausal.length; j++) {
            isNotOnRight &= rh !== directCausal[j].lh;
        }
        if (isNotOnRight) {
            last.push(rh);
        }
    }
    last.onlyUnique();

    return [first, last];
}

exports.findPairsOfSets = function (footprint, firsTrans, lastTrans) {

    var uniquePairsLeft = new Map();
    var uniquePairsRight = new Map();

    Map.prototype.hasKey = function (key) {

        console.log("\n | key: " + key);
        if (typeof key === 'string') {
            console.log(" | key is string: is found? " + this.has(key));
            return this.has(key);
        }

        console.log(" | key is not string");
        var keyFound = false;
        for (var entry of this.entries()) {

            let key_ = entry[0];
            let value_ = entry[1];
            console.log(" | map[" + key_ + "] = " + value_);
            keyFound = arraysEqual(key, key_);
            console.log(" | key is found? " + keyFound);

            if (keyFound) {
                break;
            }
        }
        return keyFound;
    }

    Map.prototype.getValue = function (key) {
        console.log("\n | key: " + key);
        if (typeof key === 'string') {
            console.log(" | key is string");
            return this.get(key);
        }
        else {
            for (var entry of this.entries()) {

                let key_ = entry[0];
                let value_ = entry[1];
                console.log(" | map[" + key_ + "] = " + value_);
                let keyFound = arraysEqual(key, key_);
                console.log(" | key is found? " + keyFound);

                if (keyFound) {
                    return entry[1];
                }
            }
        }
        return null;
    }

    /////////// a -> b ///////////////////////////////////////////////////////

    var addUpdatePair = (pairMap, key, value) => {
        if (!pairMap.hasKey(key)) {
            var arr = [value];
            pairMap.set(key, arr);
            console.log("Key not found " + key + ' => ' + util.inspect(pairMap));
        }
        else {
            var arr = pairMap.getValue(key);
            if (typeof value === 'string') {
                arr.push(value);
                console.log("(key:" + key + ', value: ' + value + ') => ' + util.inspect(pairMap));
            }
            else {
                var arrConcat = arr.concat(value);
                pairMap.set(key, arrConcat);
                console.log("(key:" + key + ', value: ' + value + ') => ' + util.inspect(paiMap));
            }
        }
    }

    var pushToX = (lh_, rh_) => {
        addUpdatePair(uniquePairsLeft, lh_, rh_);
        //addUpdatePair(uniquePairsRight, rh_, lh_);
    }

    console.log("\n\n Causaly related: ")
    var onlyCausality = footprint.filter(onlyThisRelation.bind(this, RELATION.CAUSALITY));
    for (i = 0; i < onlyCausality.length; i++) {
        console.log("" + i + " " + onlyCausality[i].lh + " " + onlyCausality[i].relation + " " + onlyCausality[i].rh);
        pushToX(onlyCausality[i].lh, onlyCausality[i].rh);
    }

    console.log("\n uniquePairsLeft : " + util.inspect(uniquePairsLeft));

    uniquePairsLeft.forEach((val, key, map) => {
        addUpdatePair(uniquePairsRight, val, key);
    });


    /////////// a # b only ///////////////////////////////////////////////////////

    // var XL = [];
    // uniquePairsLeft.forEach((val, key, map) => {
    //     var composed = val.composeElt(footprint);
    //     composed.forEach(val_ => {
    //         XL.push({
    //             lh: [key],
    //             rh: val_
    //         });
    //     });
    // });

    console.log('\n ___________________ uniquePairsRight______________\n' + util.inspect(uniquePairsRight));
    var XL0 = [];
    uniquePairsRight.forEach((val, key, map) => {
        var composed = this.composeElt(val, footprint);
        composed.forEach(val_ => {
            XL0.push({
                lh: val_,
                rh: key
            });
        });
    });

    console.log('\n ___________________ XL0 ___________________\n' + util.inspect(XL0));

    var XL = [];
    XL0.forEach(elt => {
        var composed = this.composeElt(elt.rh, footprint);
        composed.forEach(rh_ => {
            XL.push({
                lh: elt.lh,
                rh: rh_
            });
        });
    });

    console.log("\n____________________ XL _______________________\n " + JSON.stringify(XL));

    return XL;
}

exports.getFlowRelation = function (YL, TL, firstTrans, lastTrans) {

    var xI = 10, xO = 1000;
    var indexNode = -1;
    var nodes = [];
    var edges = [];

    //console.log("\n\n Nodes & Edges");

    // 1. Record transitions into Node
    var transitionMap = new Map;
    // var span = 20;
    // var yIO = 250 - ((TL.length-1) * span/2)
    for (i = 0; i < TL.length; i++) {
        indexNode++;
        var node = { 'id': indexNode, label: TL[i], group: "transition" };
        // TODO: Apply to firstTrans and lastTrans but not here
        // if (i === 0) {
        //     node['x'] = xI + span;
        //     node['y'] = yIO + i*span;
        // }
        // else if (i === TL.length-1) {
        //     node['x'] = xO - span;
        //     node['y'] = yIO + i*span;
        // }
        nodes.push(node);
        //console.info(indexNode + " - transition " + TL[i]);
        transitionMap.set(TL[i], indexNode);
    };

    // 2. Add Input Place to Nodes
    indexNode++;
    nodes.push({ 'id': indexNode, label: 'I', group: "place", x: 10, y: 250 });
    //console.info(indexNode + " - Place I ");

    // 3. Add Edge between Input Place and Input Transitions
    var indexEdge = -1;
    for (i = 0; i < firstTrans.length; i++) {
        indexEdge++;
        let nodeTo = transitionMap.get(firstTrans[i]);
        edges.push({
            id: indexEdge,
            from: indexNode,
            to: nodeTo,
            'arrows': 'to',
            'physics': false,
            'smooth': {
                'type': 'dynamic' /*'cubicBezier'*/
            }
        });
        //console.info(indexEdge + " - Edge from " + indexNode + " to " + nodeTo);
    }

    // 4. Add other Places to Nodes and
    //              Edges "from" between transitions and place,
    //              Edges "to"   between places and transition
    var placeMap = new Map;
    var p = 0;
    var q = 0;
    for (i = 0; i < YL.length; i++) {

        // 4.1 Add place
        indexNode++;
        nodes.push({ 'id': indexNode, label: 'p' + indexNode, group: "place" });

        placeMap.set(TL[i], indexNode);
        for (p = 0; p < YL[i].lh.length; p++) {
            for (q = 0; q < YL[i].rh.length; q++) {

                // 4.2. Add Edge "From"
                let nodeFrom = transitionMap.get(YL[i].lh[p]);
                indexEdge++;
                edges.push({
                    id: indexEdge,
                    from: nodeFrom,
                    to: indexNode,
                    'arrows': 'to',
                    'physics': false,
                    'smooth': {
                        'type': 'dynamic' /*'cubicBezier'*/
                    }
                });
                //console.info(indexEdge + " - Edge from " + nodeFrom + " to " + indexNode);

                // 4.2. Add Edge "To"
                let nodeTo = transitionMap.get(YL[i].rh[q]);
                indexEdge++;
                edges.push({
                    id: indexEdge,
                    from: indexNode,
                    to: nodeTo,
                    'arrows': 'to',
                    'physics': false,
                    'smooth': {
                        'type': 'dynamic' /*'cubicBezier'*/
                    }
                });
                //console.info(indexEdge + " - Edge from " + indexNode + " to " + nodeTo);
            }
        }
    };

    // 5. Add Output Place to Nodes
    indexNode++;
    nodes.push({ 'id': indexNode, label: 'O', group: "place", x: 1000, y: 250 });

    // 6. Add Edge between Ouput Transitions and Input Place
    for (i = 0; i < lastTrans.length; i++) {
        let nodeFrom = transitionMap.get(lastTrans[i]);
        indexEdge++;
        edges.push({
            id: indexEdge,
            from: nodeFrom,
            to: indexNode,
            'arrows': 'to',
            'physics': false,
            'smooth': {
                'type': 'dynamic' /*'cubicBezier'*/
            }
        });
        //console.info(indexEdge + " - Edge from " + nodeFrom + " to " + indexNode);
    }

    return [nodes, edges];
}

exports.RELATION = RELATION;
