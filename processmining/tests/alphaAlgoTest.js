const processmining = require('../alphaAlgo');

exports.testAlphaAlgo = function() {

    // var Log = [
    //     ['a', 'b', 'c', 'd'],
    //     ['a', 'c', 'b', 'd'],
    //     ['a', 'e', 'd'],
    //     // ['a', 'f']
    // ];

    // var Log = [
    //     ['a', 'b', 'e', 'f'],
    //     ['a', 'b', 'e', 'c', 'd', 'b', 'f'],
    //     ['a', 'b', 'c', 'e', 'd', 'b', 'f'],
    //     ['a', 'b', 'c', 'd', 'e', 'b', 'f'],
    //     ['a', 'e', 'b', 'c', 'd', 'b', 'f'],
    // ]

    var Log = [
        ["a","b","d","e","h"],
        ["a","d","c","e","g"],
        ["a","c","d","e","f","b","d","e","g"],
        ["a","d","b","e","h"],
        ["a","c","d","e","f","d","c","e","f","c","d","e","h"],
        ["a","c","d","e","g"]
    ]

    // Step 1: Determine each activity in L corresponding to a transition in L.
    var [TL, footprint] = processmining.getTransitionsFootPrint(Log);
    console.info("\n\nfootprint : " + JSON.stringify(footprint));
    console.info("\n\ntransitions : " + JSON.stringify(TL));

    // Step 2: Fix the set of start activities –that is, the firstelements of each trace:
    // Step 3: Fix the set of end activities –that is, elements that appear lastin a trace
    var [firstTrans, lastTrans] = processmining.getFirstLastTransitions(footprint);
    console.info("\n\n firstTrans : " + JSON.stringify(firstTrans));
    console.info("\n\n lastTrans : " + JSON.stringify(lastTrans));

    // Step 4: Calculate pairs (A, B)
    var XL = processmining.findPairsOfSets(footprint, firstTrans[0], lastTrans[0]);
    console.info("\n\n XL pairs : " + JSON.stringify(XL));

    // Step 5: Delete non-maximal pairs (A, B)
    XL.removeNonMaxPairs();
    console.info("\n\n YL pairs : " + JSON.stringify(XL));

    // Step 6: Determine places p(A, B)from pairs (A, B)

    // Step 7: Construct flow (nodes, edges)
    const [nodes, edges] = processmining.getFlowRelation(XL, TL, firstTrans, lastTrans);
    {
        console.log("\n\n Nodes & Edges :");
        nodes.forEach((node, index) => {
            console.info(index + " - " + node.group + " " + node.label);
        });

        edges.forEach((edge, index) => {
            console.info(index + " - Edge from " + edge.from + " to " + edge.to);
        });
    }

    return [nodes, edges];
}