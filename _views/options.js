var options = {
    manipulation: false,
    height: '100%',
    layout: {
      hierarchical: {
        enabled: false,
        /*levelSeparation: 100,*
        direction: "LR",
        sortMethod: "directed"*/
      }
    },
    physics: false,
    groups: {
      transition: {
        shape: "square",

      },
      place: {
        shape: "ellipse",
        scaling: {
          label: {
              enabled: true,
              min: 20,
              max: 20
          }
        },
        value: 1
      },
      token: {
        shape : 'dot',
        size : 5,
        color: "black"
      }
    }

  };
