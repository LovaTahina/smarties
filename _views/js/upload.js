
$(document).ready(function () {

    $(".file-tree").filetree({
        animationSpeed: 'fast',
        collapsed: false,
    });

    $('a[href="#CAVV_Data_Field"]').click(function (e) { // Add a click override for the folder root links
        e.preventDefault();
        var href = $(this).attr("href");
        $(this).addClass('selected');

        if ($("div" + href).attr('class') === 'closed') {
            $("div" + href).toggleClass('opened');
        }
    });

    /////////////////////////////////////////// Load all Projects //////////////////////////////////////

    $('progress').show();

    $('.blockWorkflowNet').hide();
    $(this).retrieveLogMapping(['logmapping']);

    $('progress').hide();

    /////////////////////////////////////////// Project Events ////////////////////////////////////////

    // $('#projectTitles li').click(function () {
    //     console.log($(this).attr('id'));
    //     $(this).showProject($(this).attr('id'));
    // });

    // Icon events
    $('.iconBlock > button').click(function(){
        $(this).parent().switchSubdiv(window.activeProjectId, $(this).attr("name"));
    });

    // Events on form : https://stackoverflow.com/questions/6658752/click-event-doesnt-work-on-dynamically-generated-elements
    // $("#projects").on('click', '.changePos button', function (evt) {

    $('.process').click(function () {
        $('progress').show();
        var prjProperties = {
            projectId: window.activeProjectId,
        };

        $.ajax({
            url: 'mineProcess',
            type: 'POST',
            data: prjProperties ,
            cache: false,
            success: function (dataRetrieved) {
                $('progress').hide();
            },
            failure: function (errMsg) {
                alert('Error : ' + errMsg);
                $('progress').hide();
            }
        }).done(function () {
            console.log("Success: Files sent!");
            window.location.replace("./index.html");
        }).fail(function () {
            console.log("An error occurred, the files couldn't be sent!");
            $('progress').hide();
        });
    });

    /////////////////////////////////////////// Menu Events ////////////////////////////////////////

    $('#loadButton').on('change', function () {

        $('progress').show();

        $.ajax({
            url: '/uploadLog',
            type: 'POST',

            // Form data
            data: new FormData($('form#formLoad')[0]),

            // Tell jQuery not to process data or worry about content-type
            // You *must* include these options!
            cache: false,
            contentType: false,
            processData: false,

            async: true,
            dataType: "json",
            success: function (dataRetrieved) {

                $('progress').hide();
                console.log(dataRetrieved);

                var projectDiv = null;
                var snippetDiv = null;
                var fieldPosDiv = null;
                var caseActivitiesDiv = null
                if (dataRetrieved) {
                    $('#projects').children().each(function () {
                        $(this).hide();
                    });
                    $('#projects').retrieveProjectInfo(dataRetrieved, true);
                    filelist = dataRetrieved.filelist;
                    $(this).addProjectToMenu(dataRetrieved.projectName);
                }
            },
            failure: function (errMsg) {
                alert('Error : ' + errMsg);
                $('progress').hide();
            }
        }).done(function () {
            console.log("Success: Files sent!");
            $('#evtLogMap').append($("<span class='badge'></span>").text(filelist.length));
        }).fail(function () {
            console.log("An error occurred, the files couldn't be sent!");
            $('progress').hide();
        });

    });
})