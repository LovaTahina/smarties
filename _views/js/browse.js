// <![CDATA[

    SI.Files.stylizeAll();

    /*
        --------------------------------
        Known to work in:
        --------------------------------
        - IE 5.5+
        - Firefox 1.5+
        - Safari 2+

        --------------------------------
        Known to degrade gracefully in:
        --------------------------------
    - Opera
    - IE 5.01

    --------------------------------
    Optional configuration:

    Change before making method calls.
    --------------------------------
    SI.Files.htmlClass = 'SI-FILES-STYLIZED';
    SI.Files.fileClass = 'file';
    SI.Files.wrapClass = 'cabinet';

    --------------------------------
    Alternate methods:
    --------------------------------
    SI.Files.stylizeById('input-id');
    SI.Files.stylize(HTMLInputNode);

    --------------------------------
    */

    // ]]>