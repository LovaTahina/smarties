// Return array of string values, or NULL if CSV string not well formed.
function CSVtoArray(text) {
    return text.split(';');
};