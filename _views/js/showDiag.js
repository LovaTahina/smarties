var gph = null;

function show(projectName) {
    var path = './projects/' + projectName + '/workflownet/';
    console.log(path);

    // FOOTPRINT
    $.getJSON(path + 'footprint.json', function (data) {
        if (data) {
            var transitions = data.transitions;
            var footprint = data.footprint;

            var table = $('<table></table>');
            $('<caption></caption>').text('Footprint').appendTo(table);
            var thead = $('<thead></thead>').appendTo(table);
            var theadRow = $('<tr></tr>').appendTo(thead);
            $('<th></th>').appendTo(theadRow);
            for (i = 0; i < transitions.length; i++) {
                $('<th></th>').text(transitions[i]).appendTo(theadRow);
            }
            var tbody = $('<tbody></tbody>').appendTo(table);

            for (i = 0; i < transitions.length; i++) {
                var row = $('<tr></tr>').appendTo(table);
                var cells = footprint.filter(function (fp) {
                    return fp.lh === transitions[i];
                })
                for (j = 0; j < cells.length; j++) {
                    if (j === 0) {
                        $("<td class='colname'>" + cells[j].lh + '</td>').appendTo(row);
                    }
                    // DEBUG $('<td>'+cells[j].relation+' ('+cells[j].rh+')</td>').appendTo(row);
                    $('<td>' + cells[j].relation + '</td>').appendTo(row);
                }
            }

            // Save
            table.appendTo('#footprint');
        }
    });

    // NODES
    var nodes = null;
    $.ajax({
        'async': false,
        'url': path + 'nodes.json',
        'success': function (data) {
            nodes = data;
        }
    });

    // EDGES
    var edges = null;
    $.ajax({
        'async': false,
        'url': path + 'edges.json',
        'success': function (data) {
            edges = data;
        }
    });

    var container = document.getElementById('networkTrafficCanvas');
    var data = {
        'nodes': nodes,
        'edges': edges
    }
    gph = new vis.Network(container, data, options);

    // XOR/AND split
    var callbackTrClick = function (id, label) {

        // set old style
        if (window.nodeSelected && window.nodeSelected.id) {
            gph.body.nodes[window.nodeSelected.id].options.font.color = gph.body.nodes[id].options.font.color;
            gph.body.nodes[window.nodeSelected.id].options.color = gph.body.nodes[id].options.color;
        }
        window.nodeSelected = { 'id': id, 'label': label }; // global variable
        gph.focus(id, { scale: 0.8 })

        // new style
        gph.body.nodes[id].options.font.color = '#500557';
        gph.body.nodes[id].options.color = {
            border: '#F0BA50',
            background: '#E8CB9F',
            highlight: {
                border: '#E99D13',
                background: '#E8CB9F'
            }
        };

        // Update
        gph.body.emitter.emit('_dataChanged');
        gph.redraw();

        // Retrieve attributes
        $.ajax({
            url: 'attributes.html?projectId=project5&activity=' + label,
            type: 'POST',
            data: label,
            cache: false,
            contentType: false,
            processData: false,
            success: function (dataRetrieved) {
                $('progress').hide();
                var header = ['case', 'timestamp', 'attributes']; // 'resource'];
                var properties = ['case_id', 'timestamp', 'attributes'];

                var tableInfos = $("#dataInfos").fillTable(label + " data", header, dataRetrieved, properties, callbackTrClick);
            },
            failure: function (errMsg) {
                alert('Error : ' + errMsg);
                $('progress').hide();
            }
        });
    }
    var callbackAction = function (target, params) {
        var urlTarget = target + "?projectId=project5&" + params;
        alert("OK callbackAction " + urlTarget);

        // Retrieve attributes
        $.ajax({
            url: urlTarget,
            type: 'POST',
            data: 'null',
            cache: false,
            contentType: false,
            processData: false,
            success: function (dataRetrieved) {
                $('progress').hide();
                var header = ['case', 'timestamp', 'attributes']; // 'resource'];
                var properties = ['case_id', 'timestamp', 'attributes'];
                alert('Bien ' + JSON.stringify(dataRetrieved));

                //var tableInfos = $("#dataInfos").fillTable(label + " data", header, dataRetrieved, properties, callbackTrClick);
            },
            failure: function (errMsg) {
                alert('Error : ' + errMsg);
                $('progress').hide();
            }
        });
    }

    $.getJSON(path + 'XOR_Split.json', function (data) {

        var tableXOR = $("#xor_split").fillTableAction("XOR splits",
            ['ID', 'Node'], data, ['id', 'label'], [{ label: 'learning XOR', target: 'learn_xor.html' }],
            callbackTrClick, callbackAction);
    });
    $.getJSON(path + 'XOR_Join.json', function (data) {
        var tableAND = $("#xor_join").fillTable("XOR joins",
            ['ID', 'Node'], data, ['id', 'label'], [{ label: 'learning AND', target: 'learn_and.html' }],
            callbackTrClick, callbackAction);
    });
}

$(document).ready(function () {
    $.get("activePrj", function (data, status) {
        console.log(data);
        alert(data);
        show(data);
    });
});