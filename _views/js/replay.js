$(document).ready(function () {

        $('#replay').on('change', function () {

        $('progress').show();

        $.ajax({
            url: '/replay?projectId=' + window.activeProjectId,
            type: 'POST',

            // Form data
            // data: JSON.stringify(formData),
            data: new FormData($('form#formReplay')[0]),

            // Tell jQuery not to process data or worry about content-type
            // You *must* include these options!
            cache: false,
            contentType: false,
            processData: false,

            async: true,
            dataType: "json",
            success: function (dataRetrieved) {

                loadReplay(dataRetrieved);

                // Add to menu
                var menuProject =  $('#'+window.activeProjectId+'_ID').find('ul');
                if(!menuProject.length) {
                    console.log('menuProject not found GGGG, '+$('#'+window.activeProjectId+'_ID').length);
                }
                else {
                    console.log('menuProject found OK');
                }

                var menuReplays = menuProject.find('li').filter(function() {
                    return $(this).find('a').text().indexOf( 'replays' ) > -1;
                });
                if(!menuReplays.length) {

                    menuReplays = menuProject.first().addProjectMenu('replays', window.activeProjectId, true).parent();
                }

                menuReplays.find('ul').addProjectMenu(dataRetrieved, window.activeProjectId, false);

                $('progress').hide();
            },
            failure: function (errMsg) {
                alert('Error : ' + errMsg);
                $('progress').hide();
            }
        }).done(function () {
            console.log("Success: Files sent!");
            // $('#proDisc').append($("<span class='badge'></span>").text(filelist.length));
        }).fail(function () {
            console.log("An error occurred, the files couldn't be sent!");
            $('progress').hide();
        }).always(function () {
            $("div #projects > #" + window.activeProjectId).switchSubdiv(window.activeProjectId, 'replay');
            console.log('Replay done!');
        });

    });
});