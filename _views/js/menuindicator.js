// https://freshdesignweb.com/jquery-css3-menu/
function addBadge(menuid, val) {
    var span = null;
    if (!$(menuid).has("span")) {
        console.log("span does not exist");
    } else {
        span = $(menuid + " span")
    }

    if (val === 0 && span !== null) {
        $(menuid).remove("span")
    } else {
        span.html(val);
    }
}
