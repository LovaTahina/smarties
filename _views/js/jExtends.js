var gph = null;

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

function setActiveProject(projectId0) {
    var projectId = projectId0.replace(/[^a-zA-Z0-9_ ]/g, "");
    window.activeProjectId = projectId;
    var projectIdStr = '' + window.activeProjectId;
    $("#projectTitle").text(window.activeProjectId.capitalize());
}


function loadReplay(replayJSONFile) {

    callbackConformTrClick = function (caseId, activities) {

        var nodeMissingTokens = [];
        for (var i = 0; i < window.missingTokens[caseId].length; i++) {
            nodeMissingTokens.push(window.missingTokens[caseId][i].id);
        }

        var nodeRemainingTokens = [];
        for (var i = 0; i < window.remainingTokens[caseId].length; i++) {
            nodeRemainingTokens.push(window.remainingTokens[caseId][i].id);
        }

        console.log("callbackTrClick fired case <" + caseId + ">"
            + ". Missing : " + nodeMissingTokens
            + ". Remaining : " + window.remainingTokens[caseId]
        );

        // show canevas
        $('#networkTrafficCanvas').css({ display: 'block' });
        if (nodeMissingTokens.length > 0) {
            gph.focus(nodeMissingTokens[0], { scale: 0.5 });
        }
        else if (nodeRemainingTokens.length > 0) {
            gph.focus(nodeRemainingTokens[0], { scale: 0.5 });
        }


        console.log("Nodes : " + window.nodesNbr);
        if (window.hasOwnProperty('missingNodes')) {
            gph.body.data.nodes.remove(window.missingNodes);
            delete window.missingNodes;
        }
        if (window.hasOwnProperty('remainingNodes')) {
            gph.body.data.nodes.remove(window.remainingNodes);
            delete window.remainingNodes;
        }

        /////////// Missing Tokens /////////////////////////////

        window.missingNodes = [];
        var nextId = window.nodesNbr + window.edgesNbr + 1;
        for (var n = 0; n < nodeMissingTokens.length; n++) {
            var px = gph.body.nodes[nodeMissingTokens[n]].x;
            var py = gph.body.nodes[nodeMissingTokens[n]].y;

            console.log("Next ID : " + nextId);
            window.missingNodes.push(nextId);
            gph.body.data.nodes.add({ "id": nextId, "label": "MISSING", x: px + 10, y: py + 10 });

            gph.body.nodes[nextId].options.font.color = '#420D09';
            gph.body.nodes[nextId].options.color = {
                border: '#B80F0A',
                background: '#FF0000',
                highlight: {
                    border: '#DFF280',
                    background: '#FF0000'
                }
            };

            nextId += 1;
        }

        /////////// Remaining Tokens /////////////////////////////

        window.remainingNodes = [];
        for (var n = 0; n < nodeRemainingTokens.length; n++) {
            var px = gph.body.nodes[nodeRemainingTokens[n]].x;
            var py = gph.body.nodes[nodeRemainingTokens[n]].y;

            console.log("Next ID : " + nextId);
            window.remainingNodes.push(nextId);
            gph.body.data.nodes.add({ "id": nextId, "label": "o", x: px, y: py });

            gph.body.nodes[nextId].options.font.color = '#81847d';
            gph.body.nodes[nextId].options.color = {
                border: '#81847d',
                background: '#41474a',
                highlight: {
                    border: '#41474a',
                    background: '#000000'
                }
            };

            nextId += 1;
        }

        gph.redraw();

        $([document.documentElement, document.body]).animate({
            scrollTop: $("#networkTrafficCanvas").offset().top
        }, 500);
    };

    callbackConformAction = function () {
        console.log("callbackConformAction fired");
    }

    console.log(replayJSONFile);

    // if (dataRetrieved) {
    //     $('#projects').retrieveProjectInfo(dataRetrieved, true);
    //     filelist = dataRetrieved.filelist;
    // }

    //////////////////////////////////////////////////////////////////////////////////
    var path = './projects/' + window.activeProjectId + '/replays/';
    $.getJSON(path + replayJSONFile, function (jsonStr) {

        var fitness = jsonStr.fitness;
        var data = jsonStr.data;
        console.log(JSON.stringify(data));

        // Data
        delete window.missingTokens;
        delete window.remainingTokens;

        window.missingTokens = {};
        window.remainingTokens = {};

        for (var r = 0; r < data.length; r++) {
            window.missingTokens[data[r].Case] = data[r].missings;
            window.remainingTokens[data[r].Case] = data[r].remainings;
        }
        console.log(JSON.stringify(window.missingTokens));
        console.log(JSON.stringify(window.remainingTokens));
        console.log(JSON.stringify(window.activeProjectId));

        // Table
        var title = "Conformance checking - Model fitness : " + fitness;
        var tableAND = $("div #projects > #" + window.activeProjectId + "> #replay")
        .fillTableAction(title,
            ['Case', 'Activities', 'Tokens (p: produced, c: consumed, m: missing, r: remaining)', 'Fitness'],
            data, ['Case', 'Activities', 'Metrics', 'Fitness'], [],
            callbackConformTrClick, callbackConformAction);
    }).done(function () {
        console.log("second success for replay.json");
    })
        .fail(function () {
            console.log("error at getting " + path + 'replay.json');
        })
        .always(function () {
            $("div #projects > #" + window.activeProjectId).switchSubdiv(window.activeProjectId, 'replay');
            console.log("loadReplay completed");
        });
}

function showProjectDetails(page, projectId0) {
    var projectId = projectId0.replace(/[^a-zA-Z0-9_ ]/g, "");

    // 1. Activate project
    $("div#projects").children().css({ display: 'none' });
    $("div#projects").find('#' + projectId).css({ display: 'block' });
    setActiveProject(projectId);

    // 2. Alternate project view
    $('.blockEventLog').hide();
    $('.blockWorkflownet').hide();

    // Show Log Mapping
    if (page === 'Event Log') {
        $('#blockEventLogIcons').show();
        $('div #projects > #' + projectId + '> #caseActivities').show();
    }

    // Show workflownet
    else if (page === 'workflownet') {
        $('#blockWorkflowNetIcons').show();
        $('#networkTrafficCanvas').show();
        $('body').retrieveWorkflowNet(projectId);
    }

    // Show replay
    else if (page.indexOf('replay_') !== -1) {
        $('#blockWorkflowNetIcons').show();
        $('body').retrieveWorkflowNet(projectId);
        loadReplay(page);
    }
}

function changeNodesStyle(nodeIds) {
    // // set old style
    // if (window.nodeSelecteds) {
    //     for(var i=0; i<window.nodeSelecteds.length; i++){
    //         gph.body.nodes[window.nodeSelected.id].options.font.color = gph.body.nodes[id].options.font.color;
    //         gph.body.nodes[window.nodeSelected.id].options.color = gph.body.nodes[id].options.color;
    //     }
    // }
    window.nodeSelecteds = nodeIds;
    gph.focus(nodeIds[0].id, { scale: 0.8 })

    // new style
    for (var i = 0; i < nodeIds.length; i++) {
        gph.body.nodes[nodeIds[i].id].options.font.color = '#500557';
        gph.body.nodes[nodeIds[i].id].options.color = {
            border: '#F0BA50',
            background: '#E8CB9F',
            highlight: {
                border: '#E99D13',
                background: '#E8CB9F'
            }
        };
    }

    // Update
    gph.body.emitter.emit('_dataChanged');
    gph.redraw();
}

jQuery.fn.extend({

    prepareTable: function (caption, header) {
        var tbl = $('<table></table>').attr({ id: "snippet" });
        $('<caption></caption>').text(caption).appendTo(tbl);
        var thead = $('<thead></thead>').appendTo(tbl);
        var theadRow = $('<tr></tr>').appendTo(thead);
        for (i = 0; i < header.length; i++)
            $('<th></th>').text(header[i]).appendTo(theadRow);
        var tbody = $('<tbody></tbody>').appendTo(tbl);

        // Save
        this.empty();
        tbl.appendTo(this);
        return tbl;
    },

    fillTable: function (caption, header, data, fields, callback) {
        var tbl = this.prepareTable(caption, header);
        for (var r = 0; r < data.length; r++) {
            var row = $('<tr></tr>').appendTo(tbl);
            for (var c = 0; c < fields.length; c++) {
                var infos = fields[c];
                if (typeof infos === 'string') {
                    var cell = $('<td></td>').text(data[r][fields[c]]).appendTo(row);
                }
                else if (Array.isArray(infos)) {
                    alert(infos.length);
                    for (var i = 0; i < infos.length; i++) {
                        var cell = $('<td></td>').text(data[r][infos[i]]).appendTo(row);
                    }
                }
            }
            row.click(function () {
                var id = $(this).find("td:eq(0)").text(); // get current row 1st TD value
                var label = $(this).find("td:eq(1)").text(); // get current row 2nd TD
                callback(id, label);
            })
        }
        return tbl;
    },

    fillTableAction: function (caption, header, data, fields, actions, callbackTr, callbackAction) {
        var tbl = this.prepareTable(caption, header);
        for (var r = 0; r < data.length; r++) {
            var row = $('<tr></tr>').appendTo(tbl);
            for (var c = 0; c < fields.length; c++) {
                var infos = fields[c];
                if (typeof infos === 'string') {
                    var cell = $('<td></td>').html(data[r][fields[c]]).appendTo(row);
                }
                else if (Array.isArray(infos)) {
                    alert(infos.length);
                    for (var i = 0; i < infos.length; i++) {
                        var cell = $('<td></td>').html(data[r][infos[i]]).appendTo(row);
                    }
                }
            }
            // Actions
            for (var a = 0; a < actions.length; a++) {
                var button = $("<input type=button value='" + actions[a].label +
                    "' name='" + actions[a].target + "' id='" + data[r][fields[0]] + "'>");
                var action = $('<td></td>').text(data[r][infos[i]]).appendTo(row);
                button.appendTo(action);
                button.click(function (evt) {
                    alert($(this).val() + ' ' + $(this).attr('name') + '(' + $(this).attr('id') + ')');
                    callbackAction($(this).attr('name'), 'nodeId=' + $(this).attr('id'));
                });
            }

            row.click(function () {
                var id = $(this).find("td:eq(0)").text(); // get current row 1st TD value
                var label = $(this).find("td:eq(1)").text(); // get current row 2nd TD
                callbackTr(id, label);
            })
        }
        return tbl;
    },

    prepareTableForm: function (caption, header, formClass, formId) {
        var formChange = $("<form class='" + formClass + "' id='" + formId + "'></form>");
        var tbl = $('<table></table>').attr({ id: "snippet" });
        $('<caption></caption>').text(caption).appendTo(tbl);
        var thead = $('<thead></thead>').appendTo(tbl);
        var theadRow = $('<tr></tr>').appendTo(thead);
        for (i = 0; i < header.length; i++)
            $('<th></th>').text(header[i]).appendTo(theadRow);
        var tbody = $('<tbody></tbody>').appendTo(tbl);

        // Save
        tbl.appendTo(formChange);
        formChange.appendTo(this);

        return tbl;
    },

    addFormElement: function (element, id, value, placeholder) {

        var tag = null;
        if (element === 'input') {
            tag = $("<input type='text'/>");
            tag.attr("id", id);
            tag.val(value);
            tag.appendTo(this);
        }
        else if (element === 'textarea') {
            tag = $("<textarea  rows='4' cols='50'></textarea>");
            tag.attr("id", id);
            tag.attr("placeholder", placeholder);
            tag.val(value);
            tag.appendTo(this);
        }

        return tag;
    },

    //// Project menu ////////////////////////////////////////////////////////////////
    addProjectMenu: function (menuTittle, projectId0, isSubMenu, isProjectFolder = false) {
        var projectId = projectId0.replace(/[^a-zA-Z0-9_ ]/g, "");
        var divProject = $("<li></li>");
        var link = isSubMenu ? "#" : "javascript:showProjectDetails(\"" + menuTittle + "\",\"" + projectId + "\")";
        $("<a href='" + link + "'>" + menuTittle + "</a>").appendTo(divProject);

        divProject.filetree({
            animationSpeed: 'fast',
            collapsed: false,
        });
        divProject.on('click', 'a[href="#"]', function (e) { // Add a click override for the folder root links
            e.preventDefault();
            $(this).parent().toggleClass('closed').toggleClass('open');

            return false;
        })

        if(isProjectFolder){
            divProject.attr('id', projectId+'_ID');
        }

        // Save
        divProject.appendTo(this);

        if (isSubMenu) {
            divProject.attr('class', 'folder-root closed');

            var ul = $('<ul></ul>');
            ul.appendTo(divProject);

            return ul;
        }

        return divProject;
    },

    // Project block
    addProject: function (projectId, actif) {
        var divProject = $("<div id='" + projectId + "' class='project'></div>")
        if (actif) {
            divProject.attr('style', 'display:block;');
        } else {
            divProject.attr('style', 'display:none;');
        }

        // FOR Event Log
        $("<span id='editProject' style='display: none;' class='blockEventLog'></span>").appendTo(divProject);
        $("<span id='snippet' style='display: block;' class='blockEventLog'></span>").appendTo(divProject);
        $("<span id='fieldPos' style='display: none;' class='blockEventLog'></span>").appendTo(divProject);
        $("<span id='caseActivities' style='display: none;' class='blockEventLog'></span>").appendTo(divProject);
        $("<span id='deletePrj' style='display: none;' class='blockEventLog'></span>").appendTo(divProject);

        // FOR Workflownet
        //$("<span id='networkTrafficCanvas' style='display: none;' class='blockWorkflownet'>test</span>").appendTo(divProject);
        $("<span id='xor_split' style='display: none;' class='blockWorkflownet'></span>").appendTo(divProject);
        $("<span id='xor_join' style='display: none;' class='blockWorkflownet'></span>").appendTo(divProject);
        $("<span id='dataInfos' style='display: none;' class='blockWorkflownet'></span>").appendTo(divProject);
        $("<span id='footprint' style='display: none;' class='blockWorkflownet'></span>").appendTo(divProject);
        $("<span id='replay' style='display: none;' class='blockWorkflownet'></span>").appendTo(divProject);

        $("<div class='clear'></div>").appendTo(divProject);

        // Save
        divProject.appendTo(this);

        return divProject;
    },

    // Switch subdiv, after click on Icon
    switchSubdiv: function (projectId, subdivName) {

        console.log("Switch DIV : show name " + subdivName + ' for project :' + projectId + 'found :' + $('#' + projectId).find('span#' + subdivName).length);
        // Block
        $('#' + projectId).children('span').css({ display: 'none' });
        $('#' + projectId).find('span#' + subdivName).css({ display: 'block' });

        // $('#' + projectId).children().each(function () {

        //     alert("OK " + $(this).attr('id') + ' vs ' + subdivName);
        //     if ($(this).attr('id') === subdivName) {
        //         alert("OK " + $(this).attr('id') + ' vs ' + subdivName);
        //         $(this).toggleClass('spanVisible');
        //     }
        //     else {
        //         $(this).toggleClass('spanHidden');
        //     }
        // });

        if (subdivName != 'networkTrafficCanvas') {
            $('#networkTrafficCanvas').css({ display: 'none' });
        }
        else {
            $('#networkTrafficCanvas').css({ display: 'block' });
        }
    },

    retrieveProjectInfo: function (data, actif) {

        // As Active Project
        if (actif) {
            setActiveProject(data.projectName);
        }

        // Add to DIV and hide
        projectDiv = this.addProject(data.projectName, actif);
        var id = data.projectName;
        editProjDiv = $('#projects > #' + id + '> #editProject');
        snippetDiv = $('#projects > #' + id + '> #snippet');
        fieldPosDiv = $('#projects > #' + id + ' > #fieldPos');
        caseActivitiesDiv = $('#projects > #' + id + ' > #caseActivities');
        deletePrjDiv = $('#projects > #' + id + ' > #deletePrj');

        // A. Edit project properties

        var formId = 'idFormPrjDetails';
        var csvContent = { Label: data.projectName, Description: data.description };
        var csvHtml = { Label: 'input', Description: 'textarea' };
        var tbl = editProjDiv.prepareTableForm("Project details", ["Property", "Value"], 'formPrjDetails', formId);

        // Label

        var rowLbl = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #editProject table tbody'));
        $('<td></td>').text('Label').appendTo(rowLbl);
        var cellLbl = $("<td style='vertical-align:top'></td>");
        var eltIdLbl = csvHtml.Label + data.projectName;
        var eltLbl = cellLbl.addFormElement(csvHtml.Label, eltIdLbl, csvContent.Label);
        window['temp' + eltIdLbl] = data.projectName;
        eltLbl.bind('keyup mouseup', function () {
            $(this).attr('value', $(this).val());
            console.log(eltIdLbl + " => " + $(this).attr('value'));
            window['temp' + eltIdLbl] = $(this).attr('value');
        });

        cellLbl.appendTo(rowLbl);

        // Description

        var rowDesc = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #editProject table tbody'));
        $('<td></td>').text('Description').appendTo(rowDesc);
        var cellDesc = $("<td style='vertical-align:top'></td>");
        var eltIdDesc = csvHtml.Description + data.projectName;
        var eltDesc = cellDesc.addFormElement(csvHtml.Description,
            eltIdDesc,
            csvContent.Description,
            "Add description to your project");
        window['temp' + eltIdDesc] = data.projectName;
        eltDesc.bind('keyup mouseup', function () {
            $(this).attr('value', $(this).val());
            console.log(eltIdDesc + " => " + $(this).attr('value'));
            window['temp' + eltIdDesc] = $(this).attr('value');
        });

        cellDesc.appendTo(rowDesc);

        // Button

        var button = $("<tr><td colspan='2'><input type='button' id='btnSavePrjProperties' value='Save project properties'></td></tr>");
        button.appendTo(tbl);
        button.click(function (evt) {
            evt.preventDefault();
            var prjProperties = {
                projectId: window.activeProjectId,
                label: window['temp' + eltIdLbl],
                description: window['temp' + eltIdDesc],
            };
            console.log(" ===> JSON  " + JSON.stringify(prjProperties));

            $.ajax({
                url: '/changePrjDetails',
                type: 'POST',
                data: prjProperties,
                cache: false,
                success: function (dataRetrieved) {
                    //console.log(" ===> fieldPos 3 <==== " + JSON.stringify(fieldPos));
                    $('progress').hide();
                },
                failure: function (errMsg) {
                    alert('Error : ' + errMsg);
                    $('progress').hide();
                }
            }).done(function () {
                console.log("Success: Files sent!");
                window.location.replace("./index.html");
            }).fail(function () {
                console.log("An error occurred, the files couldn't be sent!");
                $('progress').hide();
            });
        });


        // B. File snippet
        var fileSnippet = data.fileSnippet;
        if (fileSnippet) {

            var nFields = CSVtoArray(fileSnippet[0]).length;
            var numFields = [];
            for (i = 0; i < nFields; i++) {
                numFields.push(i);
            }
            snippetDiv.prepareTable("Event logs snippet", numFields);
            for (i = 0; i < fileSnippet.length; i++) {
                var row = $('<tr></tr>').appendTo($('#projects > #' + id + '> #snippet table tbody'));
                var fields = CSVtoArray(fileSnippet[i]);
                for (c = 0; c < fields.length; c++)
                    $('<td></td>').text(fields[c]).appendTo(row);
            }
        }

        // C. Field positions
        var fieldPos = data.fieldPositions;
        if (fieldPos) {
            var idFormEditFiedlPos = 'formEditFiedlPos';
            var tbl = fieldPosDiv.prepareTableForm("Field positions", ['Name', 'Position'], 'changePos', idFormEditFiedlPos);

            for (var propertyName in fieldPos) {

                if (propertyName === 'projectId') {
                    continue;
                }

                var row = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #fieldPos table tbody'));
                $('<td></td>').text(propertyName).appendTo(row);
                var cell = $('<td></td>');
                // var input = $("<input type='number' min='0' step='1' value='"+fieldPos[propertyName]+"'/>");
                var input = $("<input type='number' min='0' step='1'/>");
                input.attr("id", propertyName);
                input.val(fieldPos[propertyName]);
                window[propertyName] = fieldPos[propertyName];
                input.appendTo(cell);
                input.bind('keyup mouseup', function () {
                    $(this).attr('value', $(this).val());
                    window[$(this).attr("id")] = $(this).attr('value');
                    console.log($(this).attr("id") + " : " + $(this).attr('value'));
                });

                cell.appendTo(row);
            }

            var button = $("<tr><td colspan='2'><input type='button' id='buttonChangePos' value='Apply changes'></td></tr>");
            button.appendTo(tbl);
            button.bind('click', function (evt) {
                evt.preventDefault();
                var fieldPos = {
                    projectId: window.activeProjectId,
                    case_id: window.case_id,
                    activity_name: window.activity_name,
                    timestamp: window.timestamp,
                    resource: window.resource,
                };
                console.log(" ===> fieldPos 2 <==== " + JSON.stringify(fieldPos));

                $.ajax({
                    url: '/changeAttribPos',
                    type: 'POST',
                    data: JSON.stringify(fieldPos),
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (dataRetrieved) {
                        //console.log(" ===> fieldPos 3 <==== " + JSON.stringify(fieldPos));
                        $('progress').hide();
                    },
                    failure: function (errMsg) {
                        alert('Error : ' + errMsg);
                        $('progress').hide();
                    }
                }).done(function () {
                    console.log("Success: Files sent!");
                    window.location.replace("./index.html");
                }).fail(function () {
                    console.log("An error occurred, the files couldn't be sent!");
                    $('progress').hide();
                });
            });
        }

        // D. Fields (Case - Activities)
        var cases = data.cases;
        var activities = data.activities;
        if (cases) {
            caseActivitiesDiv.prepareTable("Fields", ['Cases', 'Activities']);

            var row = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #caseActivities table tbody'));
            var casesTxt = cases.join("<br/>");
            $('<td></td>').html(casesTxt).appendTo(row);
            var activitiesTxt = activities.join("<br/>");
            $('<td></td>').html(activitiesTxt).appendTo(row);

            $("<button id='" + id + "' class='process'></button>").appendTo(caseActivitiesDiv);

            console.info("cases : " + casesTxt + "; activities " + activitiesTxt);
        }

        // E. Trash
        deletePrjDiv.prepareTable("Fields", ['Delete project']);

        var row = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #deletePrj table tbody'));
        $('<td></td>').html('Deleting this project is permanent. Could you please confirm this action?').appendTo(row);
        row = $('<tr></tr>').appendTo($('#projects > #' + id + ' > #deletePrj table tbody'));
        var col = $('<td></td>');
        var deletePrjBtn = $("<button id='deletePrjBtn'>Confirm</button>");
        deletePrjBtn.appendTo(col);
        col.appendTo(row);

        deletePrjBtn.click(function (evt) {
            evt.preventDefault();
            console.log(" ===> Deleting " + window.activeProjectId);

            $.ajax(
                {
                    url: '/deletePrj',
                    type: 'POST',
                    data: { projectId: window.activeProjectId },
                    cache: false,
                    success: function (dataRetrieved) {
                        //console.log(" ===> fieldPos 3 <==== " + JSON.stringify(fieldPos));
                        $('progress').hide();
                    },
                    failure: function (errMsg) {
                        alert('Error : ' + errMsg);
                        $('progress').hide();
                    }
                }).done(function () {
                    console.log("Deleting project successfully");
                    window.location.replace("./index.html");
                }).fail(function () {
                    console.log("An error occurred during deletion of project");
                    $('progress').hide();
                });
        });

    },

    addProjectToMenu: function (projectId) {
        /// Load project json objects
        $.get('/projectDetails?prj=' + projectId, function (data, status) {


            // a. Add to menu
            var menu = $('.file-tree').addProjectMenu(projectId, projectId, true, true);

            // b. Add submenu and pages
            let submenuLabel = [];
            let submenuObj = [];
            for (var i = 0; i < data.length; i++) {
                let json = data[i];
                let index = -1;

                // b.1 Add submenu
                if (json.hasOwnProperty("submenu")) {
                    let s = json.submenu;
                    index = submenuLabel.indexOf(s);
                    if (index === -1) {
                        submenuObj.push(menu.addProjectMenu(s, projectId, true));
                        submenuLabel.push(s);
                        index = submenuLabel.indexOf(s);
                    }
                }

                // b.2 Add pages
                if (index !== -1) {
                    submenuObj[index].addProjectMenu(json.file, projectId, false);
                }
                else {
                    menu.addProjectMenu(json.file, projectId, false);
                }
            };
        });
    },

    /////////////////////////////////////////// Function to load all Projects //////////////////////////////////////

    retrieveLogMapping: function (listPrj, wantedProjectId = null) {
        // Clean
        $('.blockWorkflownet').hide();

        for (i = 0; i < listPrj.length; i++) {
            var path = './projects/list_' + listPrj[i] + '.json';

            $.ajaxSetup({
                async: false
            });

            var listPrjct = [];
            $.getJSON(path, function (data, status, xhr) {
                var last = (data.filelist.length - 1);
                console.log("last " + last);

                for (var j = 0; j < data.filelist.length; j++) {
                    var prj = data.filelist[j];
                    var path = './projects/' + prj.projectName + '/' + prj.filename;
                    console.log("retrieve " + path);
                    $.getJSON(path, function (data) {
                        listPrjct.push(data.projectName);

                        /// Load project info //////////////////////////////////////
                        var activate = (wantedProjectId !== null && data.projectName === wantedProjectId)
                            || (wantedProjectId === null && j === last);
                        $('#projects').retrieveProjectInfo(data, activate);
                        console.log("path " + path);

                    }).always(function () {
                        if (listPrjct.length == data.filelist.length) {

                            // window.activeProjectId = listPrjct[listPrjct.length - 1];
                            // var projectIdStr = ''+window.activeProjectId;
                            // $("#projectTitle").text(window.activeProjectId.capitalize());
                            $('#evtLogMap').append($("<span class='badge'></span>").text(data.filelist.length));
                        }
                        console.log("listPrjct.length == data.filelist.length " + (listPrjct.length == data.filelist.length))
                        console.log("listPrjct.last " + window.activeProjectId)

                        $(this).addProjectToMenu(prj.projectName);
                    });

                }
            })

            $.ajaxSetup({
                async: true
            });
        }
    },

    /////////////////////////////////////////// Function to load project WOrkflowNet //////////////////////////////////////

    retrieveWorkflowNet: function (wantedProjectId) {
        var path = './projects/' + wantedProjectId + '/workflownet/';
        console.log(path);

        // FOOTPRINT
        $.getJSON(path + 'footprint.json', function (data) {
            if (data) {
                var transitions = data.transitions;
                var footprint = data.footprint;

                var table = $('<table></table>');
                $('<caption></caption>').text('Footprint').appendTo(table);
                var thead = $('<thead></thead>').appendTo(table);
                var theadRow = $('<tr></tr>').appendTo(thead);
                $('<th></th>').appendTo(theadRow);
                for (i = 0; i < transitions.length; i++) {
                    $('<th></th>').text(transitions[i]).appendTo(theadRow);
                }
                var tbody = $('<tbody></tbody>').appendTo(table);

                for (i = 0; i < transitions.length; i++) {
                    var row = $('<tr></tr>').appendTo(table);
                    var cells = footprint.filter(function (fp) {
                        return fp.lh === transitions[i];
                    })
                    for (j = 0; j < cells.length; j++) {
                        if (j === 0) {
                            $("<td class='colname'>" + cells[j].lh + '</td>').appendTo(row);
                        }
                        // DEBUG $('<td>'+cells[j].relation+' ('+cells[j].rh+')</td>').appendTo(row);
                        $('<td>' + cells[j].relation + '</td>').appendTo(row);
                    }
                }

                // Save
                table.appendTo($('div #projects > #' + wantedProjectId + '> #footprint'));
            }
        });

        //////////////////////////////////////////////////////////////////////////////////
        // NODES
        var nodes = null;
        $.ajax({
            'async': false,
            'url': path + 'nodes.json',
            'success': function (data) {
                nodes = data;
                window.nodesNbr = nodes.length;
            }
        });

        // EDGES
        var edges = null;
        $.ajax({
            'async': false,
            'url': path + 'edges.json',
            'success': function (data) {
                edges = data;
                window.edgesNbr = edges.length;
            }
        });

        var container = document.getElementById('networkTrafficCanvas');
        var data = {
            'nodes': nodes,
            'edges': edges
        }
        gph = new vis.Network(container, data, options);

        //////////////////////////////////////////////////////////////////////////////////
        // XOR/AND split
        var callbackTrClick = function (id, label) {
            console.log('callbackTrClick with id :' + id + '; label : ' + label);

            // show canevas
            $('#networkTrafficCanvas').css({ display: 'block' });

            // set old style
            if (window.nodeSelected && window.nodeSelected.id) {
                gph.body.nodes[window.nodeSelected.id].options.font.color = gph.body.nodes[id].options.font.color;
                gph.body.nodes[window.nodeSelected.id].options.color = gph.body.nodes[id].options.color;
            }
            window.nodeSelected = { 'id': id, 'label': label }; // global variable
            gph.focus(id, { scale: 0.8 })

            // new style
            gph.body.nodes[id].options.font.color = '#500557';
            gph.body.nodes[id].options.color = {
                border: '#F0BA50',
                background: '#E8CB9F',
                highlight: {
                    border: '#E99D13',
                    background: '#E8CB9F'
                }
            };

            // Update
            gph.body.emitter.emit('_dataChanged');
            gph.redraw();

            // Retrieve attributes
            $.ajax({
                url: 'attributes.html?projectId=' + wantedProjectId + '&activity=' + label,
                type: 'POST',
                data: label,
                cache: false,
                contentType: false,
                processData: false,
                success: function (dataRetrieved) {
                    $('progress').hide();
                    var header = ['case', 'timestamp', 'attributes']; // 'resource'];
                    var properties = ['case_id', 'timestamp', 'attributes'];

                    var tableInfos = $("div #projects > #" + wantedProjectId + "> #dataInfos")
                        .fillTable(label + " data", header, dataRetrieved, properties, callbackTrClick);
                },
                failure: function (errMsg) {
                    alert('Error : ' + errMsg);
                    $('progress').hide();
                }
            });
        }
        var callbackAction = function (target, params) {
            var urlTarget = target + "?projectId=" + wantedProjectId + "&" + params;
            alert("OK callbackAction " + urlTarget);

            // Retrieve attributes
            $.ajax({
                url: urlTarget,
                type: 'POST',
                data: 'null',
                cache: false,
                contentType: false,
                processData: false,
                success: function (dataRetrieved) {
                    $('progress').hide();
                    var header = ['case', 'timestamp', 'attributes']; // 'resource'];
                    var properties = ['case_id', 'timestamp', 'attributes'];
                    alert('Bien ' + JSON.stringify(dataRetrieved));

                    //var tableInfos = $("#dataInfos").fillTable(label + " data", header, dataRetrieved, properties, callbackTrClick);
                },
                failure: function (errMsg) {
                    alert('Error : ' + errMsg);
                    $('progress').hide();
                }
            });
        }

        $.getJSON(path + 'XOR_Split.json', function (data) {

            var tableXOR = $("div #projects > #" + wantedProjectId + "> #xor_split").fillTableAction("XOR splits",
                ['ID', 'Node'], data, ['id', 'label'], [{ label: 'learning XOR', target: '/learn_xor' }],
                callbackTrClick, callbackAction);
        }).done(function () {
            console.log("second success for XOR_Split.json");
        })
            .fail(function () {
                console.log("error at getting " + path + 'XOR_Join.json');
            })
            .always(function () {
                console.log("complete");
            });

        $.getJSON(path + 'XOR_Join.json', function (data) {
            var tableAND = $("div #projects > #" + wantedProjectId + "> #xor_join").fillTableAction("XOR joins",
                ['ID', 'Node'], data, ['id', 'label'], [{ label: 'learning AND', target: '/learn_and' }],
                callbackTrClick, callbackAction);
        }).done(function () {
            console.log("second success for XOR_Join.json");
        })
            .fail(function () {
                console.log("error at getting " + path + 'XOR_Join.json');
            })
            .always(function () {
                console.log("complete");
            });
    }
});

