
///////////////////////////////////////////////////////////////////////////////////////

class Item {
    constructor(lbl, freq) {
        this._label = lbl;
        this._freq = freq;
    }
};

class Transaction {
    constructor(lbl, items) {
        _label = lbl;
        _items = items;
    }
};

A = new Item('A', 1);
C = new Item('C', 2);
D = new Item('D', 1);
E = new Item('E', 4);
I = new Item('I', 1);
K = new Item('K', 5);
M = new Item('M', 3);
N = new Item('N', 2);
O = new Item('O', 3);
U = new Item('U', 1);
Y = new Item('Y', 3);
T1 = [E, K, M, N, O, Y];
T2 = [D, E, K, N, O, Y];
T3 = [A, E, K, M];
T4 = [C, K, M, U, Y];
T5 = [C, E, I, K, O, O];

L = [A, C, D, E, I, K, M, N, O, U, Y];
thresold = 3;

Array.prototype.removeIf = function(callback) {
    var i = this.length;
    while (i--) {
        if (callback(this[i], i)
        || this.indexOf(this[i]) != i) {
            this.splice(i, 1);
        }
    }
};

function sortItem(item, thresold) {
    item.sort(function (a, b) {
        return (a._freq > b._freq) ? -1 : 1;
    });
    item.removeIf(a => a._freq < thresold);
}

sortItem(L,thresold);
sortItem(T1, thresold);
sortItem(T2, thresold);
sortItem(T3, thresold);
sortItem(T4, thresold);
sortItem(T5, thresold);

class FPTree {
    constructor(){
        this.childs = []
    }
}