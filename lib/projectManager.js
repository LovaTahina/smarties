'use strict';
const fs = require('fs');
const path = require('path');

const projectsDir = 'projects';
const listFile = 'list_';
const projectDefault = 'project';
const projectsPath = './' + projectsDir + '/';
const counterFile = projectsPath + 'counter.txt';
const workflownetDir = '/workflownet/'
const nodesFile = '/nodes.json'
const edgesFile = '/edges.json'
const XOR_Split_File = '/XOR_Split.json'
const XOR_Join_File = '/XOR_Join.json'
const activityDir = '/activities/';
const listLogMappingFile = 'list_logmapping.json'
const replayDir = '/replays/'
const replayFile = 'replay_';
const logMappingFile = '/logmapping.json'
const description = 'description';

const hashids = require('./hashids');



// https://stackoverflow.com/questions/18052762/remove-directory-which-is-not-empty
const deleteFolderRecursive = function (dataPath) {
    if (fs.existsSync(dataPath)) {
        fs.readdirSync(dataPath).forEach((file, index) => {
            const curPath = path.join(dataPath, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(dataPath);
    }
};

// https://gist.github.com/kethinov/6658166
function findInDir(dir, filter, fileList = []) {
    const files = fs.readdirSync(dir);

    files.forEach((file) => {
        const filePath = path.join(dir, file);
        const fileStat = fs.lstatSync(filePath);

        if (fileStat.isDirectory()) {
            findInDir(filePath, filter, fileList);
        } else if (filter.test(filePath)) {
            fileList.push(filePath);
        }
    });

    return fileList;
}



function getProjectDetails(filename) {
    var projectName = path.dirname(filename).replace(projectsDir, '').replace("\\", '');
    return { 'projectName': projectName, 'filename': path.basename(filename) }
}

function getNextProjectId() {
    var counter = 0;

    if (fs.existsSync(counterFile)) {
        counter = fs.readFileSync(counterFile);
    }
    counter++;
    fs.writeFileSync(counterFile, counter);

    return counter;
}

function updateProjectListName(filename, filelist) {
    var objFilelist = { filelist: filelist };
    fs.writeFileSync(projectsPath + listFile + filename, JSON.stringify(objFilelist));
}


exports.createProject = function () {
    var projectName = projectDefault + getNextProjectId();
    var path = projectsPath + projectName;

    if (!fs.existsSync(path)) {
        fs.mkdirSync(path);
    }

    return projectName;
}

exports.changeProjectProperties = function (req, res) {

    // Change JSON value into project list
    let listLogMappingPath = projectsPath + listLogMappingFile;
    var file_content = fs.readFileSync(listLogMappingPath);
    var content = JSON.parse(file_content);
    var refObj = content.filelist.filter(obj => obj.projectName === req.body.projectId);
    refObj[0].projectName = req.body.label.replace(/[^a-zA-Z0-9_ ]/g, "");
    fs.writeFileSync(listLogMappingPath, JSON.stringify(content));

    // Change JSON value into logmapping
    let logMappingPath = projectsPath + req.body.projectId + logMappingFile;
    var file_content = fs.readFileSync(logMappingPath);
    var content = JSON.parse(file_content);
    content.projectName = req.body.label.replace(/[^a-zA-Z0-9_ ]/g, "");
    content.fieldPositions.projectId = req.body.label.replace(/[^a-zA-Z0-9_ ]/g, "");
    content[description] = req.body.description;
    fs.writeFileSync(logMappingPath, JSON.stringify(content));

    // Rename folder
    if (fs.existsSync(projectsPath + req.body.projectId)) {
        fs.renameSync(projectsPath + req.body.projectId, projectsPath + req.body.label, (err) => {
            if (err) {
                throw err;
            }
            fs.statSync(projectsPath + req.body.label, (error, stats) => {
                if (error) {
                    throw error;
                }
                console.log(`stats: ${JSON.stringify(stats)}`);
            });
        });
    }

    res.end('done');
}

exports.reCreateDataDir = function (projectName, dataDir) {
    var dataPath = projectsPath + projectName + dataDir;

    try {
        if (fs.existsSync(dataPath)) {
            deleteFolderRecursive(dataPath);
        }
        fs.mkdirSync(dataPath);
    }
    catch (err) {
        console.log("ERROR: while trying to reCreateDataDir. " + err);
    }
}

exports.deleteProject = function (projectId) {

    // Change JSON value into list
    let listLogMappingPath = projectsPath + listLogMappingFile;
    var file_content = fs.readFileSync(listLogMappingPath);
    var content = JSON.parse(file_content);
    var filteredContent = content.filelist.filter(obj => obj.projectName !== projectId);
    fs.writeFileSync(listLogMappingPath, JSON.stringify({ filelist: filteredContent }));

    // Delete project directory
    var dataPath = projectsPath + projectId;
    try {
        if (fs.existsSync(dataPath)) {
            deleteFolderRecursive(dataPath);
        }
    }
    catch (err) {
        console.log("ERROR: while trying to deleteProject. " + err);
    }
}

exports.encodeFilename = function (filename) {
    var encoded = null;
    try {
        if (filename) {
            var hex = Buffer.from(filename, 'utf8').toString('hex');

            var hash = new hashids.Hashids();
            encoded = hash.encodeHex(hex);
        }
    }
    catch (err) {
        console.log(err);
    }

    return encoded;
}

exports.decodeFilename = function (filename) {
    var decoded = null;
    try {
        var hash = new hashids.Hashids();
        var hex = hash.decodeHex(filename);
        decoded = hex.toString('hex');
    }
    catch (err) {
        console.log(err);
    }

    return decoded;
}

exports.getDataInfo = function (projectName, dataDir, filename) {
    return this.loadJSON(projectName, dataDir + filename + '.json');
}

exports.saveProject = function (infos, filename, additionalData) {
    var path = projectsPath + infos.projectName + '/';
    if (!fs.existsSync(path)) {
        console.error("ERROR: missing project folder " + path);
        return false;
    }

    try {
        var filePath = path + filename;
        fs.writeFileSync(filePath, JSON.stringify(infos));

        for (var i = 0; i < additionalData.length; i++) {
            filePath = path + additionalData[i].filename;
            fs.writeFileSync(filePath, JSON.stringify(additionalData[i].data));
        }

        // to IHM
        var projectListName = this.loadProjectListName(filename);
        updateProjectListName(filename, projectListName);
        return projectListName;
    }
    catch (e) {
        console.error("ERROR: can't write data into " + filePath + '. ' + e);
    }
}

exports.getProjectDetails = function (projectId, res) {

    // Get json list
    var jsonList = [];
    var path = projectsPath + projectId;
    var filelist = findInDir(path, new RegExp(".json"));
    filelist.forEach(file => {
        let infos = file.split(projectId)[1].split(/[\\,\/]+/);
        console.info(" > " + file + ' => ' + infos);
        let json = {};
        let i = 1;
        let bAdd = false;
        if (infos.length === 3) {
            var folder = infos[i];
            if (workflownetDir.indexOf(folder) !== -1) {
                json['submenu'] = folder;
            }
            else if (replayDir.indexOf(folder) !== -1) {
                json['submenu'] = folder;
            }
            i++;
        }

        var file = infos[i];

        if (logMappingFile.indexOf(file) !== -1) {
            console.log(" logMappingFile :" + logMappingFile + " indexOf(" + file + ") " + logMappingFile.indexOf(file));
            json['file'] = 'Event Log';
            bAdd = true;
        }
        else if (nodesFile.indexOf(file) !== -1) {
            console.log(" nodesFile :" + nodesFile + " indexOf(" + file + ") " + nodesFile.indexOf(file));
            json['file'] = 'workflownet';
            bAdd = true;
        }
        else if (file.indexOf(replayFile) !== -1) {
            console.log(" replayFile :" + nodesFile + " indexOf(" + file + ") " + nodesFile.indexOf(file));
            json['file'] = file;
            bAdd = true;
        }

        if (bAdd){
            console.log("Ajout de " + JSON.stringify(json) );
            jsonList.push(json);
        }

    });

    // Response
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(jsonList));
}

exports.loadProjectListName = function (filename) {
    // Usage
    var jsonList = [];
    var filelist = findInDir(projectsPath, new RegExp(filename));
    filelist.forEach(file => {
        console.info(" > " + file);
        // if(!file.includes(listFile))
        // {
        jsonList.push(getProjectDetails(file));
        // }
    });

    return jsonList;
}

exports.getDataPathForProject = function (projectName) {
    return projectsPath + projectName + '/data.txt';
}

exports.loadJSON = function (projectName, jsonfile) {
    var path = projectsPath + projectName + jsonfile;
    var json = null;
    try {
        json = JSON.parse(fs.readFileSync(path));
    }
    catch (e) {
        console.log("ERROR: while trying to load Case-activities. " + e);
    }

    return json;
}

/********************** Workflow-net *********************/

exports.loadNodes = function (projectName) {
    return this.loadJSON(projectName, workflownetDir + nodesFile);
}

exports.loadEdges = function (projectName) {
    return this.loadJSON(projectName, workflownetDir + edgesFile);
}

exports.loadAND = function (projectName) {
    return this.loadJSON(projectName, workflownetDir + XOR_Join_File);
}

exports.loadXOR = function (projectName) {
    return this.loadJSON(projectName, workflownetDir + XOR_Split_File);
}

exports.saveModel = function (projectName, modelName, model) {
    var path = projectsPath + projectName + '/models/';

    try {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }

        var stream = fs.createWriteStream(path + modelName + ".json", { flags: 'w' });
        var nodesData = JSON.stringify(model);
        stream.write(nodesData + "\n");
        stream.end();
    }
    catch (e) {
        console.log("ERROR: while trying to save model. " + e);
    }
}

exports.saveReplay = function (projectName, replayArrayOut, epoc) {
    var path = projectsPath + projectName + '/replays/';

    try {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        var filename = "replay_" + epoc + ".json";
        var stream = fs.createWriteStream(path + filename, { flags: 'w' });
        var nodesData = JSON.stringify(replayArrayOut);
        stream.write(nodesData + "\n");
        stream.end();

        return filename;
    }
    catch (e) {
        console.log("ERROR: while trying to save replay. " + e);
    }

    return "";
}

exports.saveWorkflowNet = function (projectName, footprint, workflownet) {
    var path = projectsPath + projectName + workflownetDir;

    try {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }

        var stream = fs.createWriteStream(path + "footprint.json", { flags: 'w' });
        var nodesData = JSON.stringify(footprint);
        stream.write(nodesData + "\n");
        stream.end();

        var stream = fs.createWriteStream(path + nodesFile, { flags: 'w' });
        var nodesData = JSON.stringify(workflownet.nodes);
        stream.write(nodesData + "\n");
        stream.end();

        stream = fs.createWriteStream(path + edgesFile, { flags: 'w' });
        var edgesData = JSON.stringify(workflownet.edges);
        stream.write(edgesData + "\n");
        stream.end();

        stream = fs.createWriteStream(path + XOR_Split_File, { flags: 'w' });
        var nodesData = JSON.stringify(workflownet.XOR_Split);
        stream.write(nodesData + "\n");
        stream.end();

        stream = fs.createWriteStream(path + XOR_Join_File, { flags: 'w' });
        var edgesData = JSON.stringify(workflownet.XOR_Join);
        stream.write(edgesData + "\n");
        stream.end();

        return true;
    }
    catch (e) {
        console.log("ERROR: while trying to save workflownet. " + e)
    }

    return false;
}

exports.activityDir = activityDir;