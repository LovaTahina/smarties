'use strict';
const fs = require('fs');
const fieldPositions = require('./events');
const csv2json = require('./csv2json');
const projectManager = require('./projectManager');

///////////////////////////////////////////////////////////////////////////

const FILE_TYPE = {
    CSV: ';',
    SEPAR_TAB: '\t',
    FIXE: '.',
    STRUCTURED: '?'
}

const OPERATION = {
    CASES_ACTIVITIES: 0,
    REPLAY: 1,
}

const nMax = 20;

///////////////////////////////////////////////////////////////////////////

Array.prototype.allTheSameObj = function (predic) {
    var i = this.length;
    while (i--) {
        if (this.findIndex(predic.bind(this, this[i])) != i) {
            this.splice(i, 1);
        }
    }
    return this.length === 1;
};

Array.prototype.allTheSameLength = function (predic) {
    var i = this.length;
    var initialLength = 0;
    var nbChange = -1;
    while (i--) {
        let actualLength = this[i].length;
        if (actualLength !== initialLength) {
            initialLength = actualLength;
            nbChange++;
        }
    }
    //return initialLength !== 0 && nbChange <= 1;

    return true;
};

Array.prototype.onlyUnique = function () {
    var i = this.length;
    while (i--) {
        if (this.indexOf(this[i]) != i) {
            this.splice(i, 1);
        }
    }
};

Array.prototype.onlyUniqueObj = function (predic) {
    var i = this.length;
    while (i--) {
        if (this.findIndex(predic.bind(this, this[i])) != i) {
            this.splice(i, 1);
        }
    }
};

String.prototype.getIndexesOf = function (a) {
    var indexes = [], i = -1;
    while ((i = this.indexOf(a, i + 1)) != -1) {
        indexes.push(i);
    }
    return indexes;
};

Array.prototype.removeIf = function (pred, item) {
    var i = this.length - 1;
    var found = false;
    while (!found && i >= 0) {
        if (pred(this[i], item)) {
            this.splice(i, 1);
            found = true;
        }
        i--;
    }

    return found;
}

///////////////////////////////////////////////////////////////////////////

const unique = (value, index, self) => {
    return self.indexOf(value) === index;
};

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length != b.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.

    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}

var json2caseActivities = function (events, cases, activitiesCanditates) {
    var casesActivities = {}

    events.forEach(element => {
        // To Server
        if (element.case_id !== undefined && element.activity_name !== undefined) {
            if (casesActivities[element.case_id] === undefined) {
                casesActivities[element.case_id] = new Array();
            }
            casesActivities[element.case_id].push(element.activity_name);

            // To IHM
            activitiesCanditates.push(element.activity_name);
            cases.push(element.case_id);
        }
    });

    //console.log(JSON.stringify(casesActivities));
    return casesActivities;
};

var activitiesAttributes = function (data, cases, activitiesCanditates) {
    var casesActivities = {}

    // TODO: faire un {case_id, attributes};

    events.forEach(element => {
        // To Server
        if (element.case_id !== undefined && element.activity_name !== undefined) {
            if (casesActivities[element.case_id] === undefined) {
                casesActivities[element.case_id] = new Array();
            }
            casesActivities[element.case_id].push(element.activity_name);

            // To IHM
            activitiesCanditates.push(element.activity_name);
            cases.push(element.case_id);
        }
    });

    //console.log(JSON.stringify(casesActivities));
    return casesActivities;
};

function computeFitness(m, c, r, p) {
    var f= (1 - (m / c + r / p) / 2);
    return f.toFixed(3);
}

function replayEvents(res, retrievedData, infos) {

    console.log("\n\n replayEvents");

    try {
        // 1. Get activities from events
        var cases = [];
        var activities = [];
        var replayArrayOut = [];

        var casesActivities = json2caseActivities(retrievedData.casesActivities, cases, activities);
        var nodes = projectManager.loadNodes(infos.projectName);
        var edges = projectManager.loadEdges(infos.projectName);
        var XOR = projectManager.loadXOR(infos.projectName);
        var AND = projectManager.loadAND(infos.projectName);
        var datasets = {};
        for (var i = 0; i < XOR.length; i++) {
            datasets['nodeid'] = XOR[i].id;
        }

        // 2. helper functions for nodes
        var getNodesOut = function (actualTransitId) {

            var findEdgeFrom = (from, elt) => {
                return elt.from === from;
            }
            var nextNodeIds = edges.filter(findEdgeFrom.bind(this, actualTransitId))
                .map(function (obj) {
                    return obj.to;
                });
            nextNodeIds.onlyUnique();

            var nextNodes = [];
            nextNodeIds.forEach(id => {
                nextNodes.push(nodes[id]);
            });

            return nextNodes;
        }
        var getNodesIn = function (actualTransitId) {

            var findEdgeTo = (to, elt) => {
                return elt.to === to;
            }
            var nextNodeIds = edges.filter(findEdgeTo.bind(this, actualTransitId))
                .map(function (obj) {
                    return obj.from;
                });
            nextNodeIds.onlyUnique();

            var prevNodes = [];
            nextNodeIds.forEach(id => {
                prevNodes.push(nodes[id]);
            });

            return prevNodes;
        }

        var sameNodes = function (lh, rh) {
            return lh.id === rh.id;
        }

        // 3. helper functions for tokens
        var produceTokens = function (tokens, nodesOut, metrics) {
            metrics.p += nodesOut.length;
            return tokens.concat(nodesOut);
        }

        var addMissingTokens = function (missings, tokens, nodesOut, metrics) {
            nodesOut.forEach(node => {
                if (tokens.findIndex(sameNodes.bind(this, node)) === -1) {
                    console.log("\t - missing token : " + JSON.stringify(node));
                    tokens.push(node);
                    missings.push(node);
                    metrics.m++;
                }
            })
        }

        var setRemainingTokens = function (tokens, metrics) {
            metrics.r = tokens.length;
        }

        var fire = function (tokens, requiredNodesIn, metrics) {
            var fired = true;
            for (var n = 0; fired && n < requiredNodesIn.length; n++) {
                fired = tokens.removeIf(sameNodes, requiredNodesIn[n]);
            }
            if (fired) {
                metrics.c += requiredNodesIn.length;
            }
            return fired;
        }


        // 3. Process
        var missingTokens = {};
        var sumM = 0;
        var sumC = 0;
        var sumR = 0;
        var sumP = 0;
        for (var caseTmp in casesActivities) {

            var replayJSON = {};
            var missings = [];

            var metrics = {
                p: 0,
                c: 0,
                m: 0,
                r: 0,
            };

            // 3.1 Activities (trace) for this case
            let activities = casesActivities[caseTmp];
            replayJSON['Case'] = caseTmp;
            replayJSON['Activities'] = activities;
            console.log("\n ------- trace " + JSON.stringify(activities) + ' ------------')

            var tokens = [];

            // 3.2 New token on initial place
            var nodeI = nodes.filter(node => { return node.label === 'I'; });
            tokens = produceTokens(tokens, nodeI, metrics);

            // 3.3 Replay
            for (var a = 0; a < activities.length; a++) {
                let activity = activities[a];
                console.log("\n Next activity [" + activity + "]");
                var activityFound = false;

                // 3.3.1 Find next activity
                var t = 0;
                console.log("\t tokens " + JSON.stringify(tokens));

                while (!activityFound && t < tokens.length) {

                    // a. Play a token
                    let nodesOut = getNodesOut(tokens[t].id);
                    console.log("\t tokens[" + t + "] " + JSON.stringify(tokens));
                    console.log("\t possible transitions " + JSON.stringify(nodesOut));

                    for (var n = 0; !activityFound && n < nodesOut.length; n++) {
                        let node = nodesOut[n];

                        // a.1 Activity found
                        if (node.label === activity) {

                            activityFound = true;
                            let transition = node;

                            // a.1.1 Try to fire
                            console.log("\t firing transition " + JSON.stringify(transition));
                            let requiredNodesIn = getNodesIn(transition.id);
                            addMissingTokens(missings, tokens, requiredNodesIn, metrics);
                            if (fire(tokens, requiredNodesIn, metrics)) {
                                let nextNodes = getNodesOut(transition.id);

                                tokens = produceTokens(tokens, nextNodes, metrics);


                            }
                        }

                    }

                    // a.2 Activity not found, try with other token
                    t++;
                }

            };

            // So far
            var nodeO = nodes.filter(node => { return node.label === 'O' });
            fire(tokens, nodeO, metrics);
            setRemainingTokens(tokens, metrics);

            var smetrics = 'p : ' + metrics.p + '<br/> c : ' + metrics.c + '<br/>';
            var red = "<span style='color: red; font-weight: bold'>";

            if (metrics.m > 0)
                smetrics += red;
            smetrics += ' m : ' + metrics.m;
            if (metrics.m > 0)
                smetrics += "</span>";
            smetrics += '<br/>';

            if (metrics.r > 0)
                smetrics += red;
            smetrics += ' r : ' + metrics.r;
            if (metrics.mr > 0)
                smetrics += "</span>";

            replayJSON['Metrics'] = smetrics;
            replayJSON['missings'] = missings;
            replayJSON['remainings'] = tokens;
            replayJSON['Fitness'] = computeFitness(metrics.m, metrics.c, metrics.r, metrics.p);

            sumM += metrics.m;
            sumC += metrics.c;
            sumR += metrics.r;
            sumP += metrics.p;

            console.assert(tokens.length === 0, "Remaining tokens" + JSON.stringify(tokens));
            console.info(" \t ===[ Metrics: " + JSON.stringify(metrics) + ' ]===');

            replayArrayOut.push(replayJSON);
        };

        // 4. Save
        const epoc = Date.now();
        const filename = projectManager.saveReplay(infos.projectName,
            { 'fitness': computeFitness(sumM, sumC, sumR, sumP), 'data': replayArrayOut, 'epoc': epoc }, epoc);
        // To IHSM
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(filename));
    }
    catch (e) {
        console.log('ERROR: while trying to replay events. ' + e)
    }

}

function writeResponse(res, retrievedData, infos, activityMap) {

    var cases = [];
    var activities = [];

    var casesActivities = json2caseActivities(retrievedData.casesActivities, cases, activities);
    infos['cases'] = cases.filter(unique);
    infos['activities'] = activities.filter(unique);

    // To Server
    var additionalData = [
        { filename: 'casesActivities.json', data: casesActivities },
    ];

    var dirData = projectManager.activityDir;
    projectManager.reCreateDataDir(infos.projectName, dirData);
    for (var entry of activityMap.entries()) {
        var key = entry[0],
            value = entry[1];
        var filename0 = '' + key;
        filename0 = filename0.trim();
        var filename = projectManager.encodeFilename(filename0);
        console.log("key: <" + filename0 + ">, filename: <" + filename + ">");
        additionalData.push({ filename: dirData + filename + '.json', data: value });

        projectManager.decodeFilename(filename);
    }

    var filelist = projectManager.saveProject(infos, 'logmapping.json', additionalData);
    infos['filelist'] = filelist;

    // To IHSM
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(infos));
}

///////////////////////////////////////////////////////////////////////////

exports.readEventLog = function (req, res, projectName, operation) {
    var minColumnNbr = 0;
    var separatorIndexes = [];
    var remaining = '';
    var fileContent = '';
    var isHeader = true;

    var fileSnippet = [];
    var nActual = 0;

    var fileFormat = null;
    var datafile = projectManager.getDataPathForProject(projectName);
    var stream = fs.createWriteStream(datafile);

    var positions = null;
    var retrievedData = { casesActivities: [], attributes: [] };
    var activityMap = new Map;

    try {
        switch (operation) {
            case OPERATION.REPLAY:
                let logmapping = projectManager.loadJSON(projectName, '/logmapping.json');
                positions = logmapping.fieldPositions;
                break;

            default:
                positions = fieldPositions;
                break;
        }

        req.on('data', function (data) {
            remaining += data;
            fileContent += data;
            // console.log(JSON.stringify(data));

            // Sanitize : remove http header
            {
                var indexHeader = remaining.indexOf('\r\n\r\n');
                if (isHeader && indexHeader < 0) {
                    return;
                }
                else if (indexHeader > -1) {
                    remaining = remaining.substring(indexHeader + 4);
                    isHeader = false;
                    remaining = remaining.replace('\r', '');
                }
            }

            var index = remaining.indexOf('\n');
            var last = 0;
            while (index > -1) {
                let line = remaining.substring(last, index);
                line = line.replace('\r', '');

                // remove footer
                var remained = line.split('-----------------------------');
                if (remained.length > 0) {
                    line = remained[0];
                }

                stream.write(line + '\n');

                if (!line.startsWith('-----------------------------')) {
                    let separatorIndexe = line.getIndexesOf(';');
                    separatorIndexes.push(separatorIndexe);
                    // DEGUB:
                    //console.log(JSON.stringify(separatorIndexe));

                    // Define File type
                    if (separatorIndexes.allTheSameLength()) {
                        //console.info("\n\n File format : CSV");

                        fileFormat = FILE_TYPE.CSV;

                        // Parse file
                        let [objEvent, length] = csv2json(line, positions, activityMap);
                        if (!minColumnNbr || (minColumnNbr && length < minColumnNbr)) {
                            minColumnNbr = length;
                        }
                        retrievedData.casesActivities.push(objEvent);
                        // DEGUB:
                        //console.log(objEvent);
                    }
                    else {
                        fileFormat = FILE_TYPE.STRUCTURED;
                    }

                    // Get snippet
                    if (nActual < nMax) {
                        fileSnippet.push(line);
                        nActual++;
                    }

                }
                last = index + 1;
                index = remaining.indexOf('\n', last);
            }

            remaining = remaining.substring(last);
        });

        req.on('end', function () {
            var detected = false;

            if (remaining.length > 0) {
                let separatorIndexe = remaining.getIndexesOf(';');
                if (!minColumnNbr || (minColumnNbr && separatorIndexe < minColumnNbr)) {
                    minColumnNbr = separatorIndexe;
                }
                separatorIndexes.push(separatorIndexe);
                // DEGUB:
                //console.log(JSON.stringify(separatorIndexe));

                // Define File type
                if (separatorIndexes.allTheSameLength()) {
                    //console.info("\n\n File format : CSV");

                    fileFormat = FILE_TYPE.CSV;
                    detected = true;

                    // Parse file
                    let [objEvent, length] = csv2json(line, positions, activityMap);
                    if (!minColumnNbr || (minColumnNbr && length < minColumnNbr)) {
                        minColumnNbr = length;
                    }
                    retrievedData.casesActivities.push(objEvent);
                    // DEGUB:
                    //console.log(objEvent);
                }
                else {
                    fileFormat = FILE_TYPE.STRUCTURED;
                }

                // remove footer
                var remained = remaining.split('-----------------------------');
                if (remained.length > 0) {
                    remaining = remained[0];
                }
                remaining = remaining.replace('\r', '');
                stream.write(remaining + '\n');
            }

            var infos = {
                'projectName': projectName,
                'fileSnippet': fileSnippet,
                'fieldPositions': positions,
                'fileFormat': fileFormat,
                'detected': detected,
                'columnNumber': minColumnNbr,
            }

            switch (operation) {
                case OPERATION.CASES_ACTIVITIES:
                    writeResponse(res, retrievedData, infos, activityMap);
                    break;
                case OPERATION.REPLAY:
                    replayEvents(res, retrievedData, infos, activityMap);
                    break;

                default:
                    break;
            }

            // DEGUB: res.end(JSON.stringify(fileContent));

            //console.log(fileContent);
            stream.close();
        });
    }
    catch (e) {
        console.error("ERROR: while trying to read event logs. " + e);
    }
};

exports.getCasesActivities = function (projectName, datafile, positions, res) {
    var stream = fs.createReadStream(datafile);
    var fileSnippet = [];
    var remaining = '';
    var nActual = 0;
    var minColumnNbr = 0;

    var retrievedData = { casesActivities: [], attributes: [] };
    var activityMap = new Map;

    stream.on('data', function (data) {
        //data = Buffer.from(data); // WARNING: uncommenting this line fixes things somehow
        remaining += data;
        var index = remaining.indexOf('\n');
        var last = 0;
        while (index > -1) {
            let line = remaining.substring(last, index);

            // Parse file
            if (line.length > 0) {
                let [objEvent, length] = csv2json(line, positions, activityMap);
                if (!minColumnNbr || (minColumnNbr && length < minColumnNbr)) {
                    minColumnNbr = length;
                }
                retrievedData.casesActivities.push(objEvent);

                // Get snippet
                if (nActual < nMax) {
                    fileSnippet.push(line);
                    nActual++;
                }
            }

            last = index + 1;
            index = remaining.indexOf('\n', last);
        }

        remaining = remaining.substring(last);
    });

    stream.on('end', function () {

        var infos = {
            'projectName': projectName.replace(/[^a-zA-Z0-9_ ]/g, ""),
            'fileSnippet': fileSnippet,
            'fieldPositions': positions,
            'fileFormat': FILE_TYPE.CSV,
            'detected': true,
            'columnNumber': minColumnNbr,
        }

        writeResponse(res, retrievedData, infos, activityMap);
    });
}


exports.FILE_TYPE = FILE_TYPE;
exports.OPERATION = OPERATION;