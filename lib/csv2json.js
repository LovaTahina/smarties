
module.exports = function (csv, eventPosition, activityMap, separator = ';') {
    //console.log(eventPosition);
    var attributes = csv.split(separator);
    var attributesObject = {};


    //console.log(dataArray);
    var result = {};
    for(const key in eventPosition) {
        result[key] = attributes[eventPosition[key]];
        attributesObject[key] = attributes[eventPosition[key]];
    }

    let activity_name = result['activity_name'];
    if(!activityMap.has(activity_name)){
        activityMap.set(activity_name, []);
    }

    attributesObject['attributes'] = attributes;
    activityMap.get(activity_name).push( attributesObject );

    //console.log(result);
    return [result, attributes.length];
}
