// https://github.com/hiddentao/linear-algebra
const linear_algebra = require('../algebra/linear-algebra.min.js')(),
    // initialise it
    Vector = linear_algebra.Vector,
    Matrix = linear_algebra.Matrix;

const Model = require('../model');
const learning = require('../learning');
const logistic = require('../logisticRegression');
const sample1 = require('../data/sample1');
const sample2 = require('../data/sample2');

exports.testLogisticRegression = function() {
    var J = [];
    var cost0;
    var J_grad;

    // TEST 1
    {
        var iter = 1;
        var lambda = 0;
        theta = new Matrix([-24, 0.2, 0.2]);

        var model = learning.learningRegression(logistic.computeCostGrad, theta, sample1.X, sample1.Y, lambda, iter);

        console.info("J : " + JSON.stringify(model._costFunction) + ' --- Expected cost (approx): 0.218')
        console.info("Grad : " + JSON.stringify(model._gradient) + ' --- Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n');
        console.info('Theta : ' + JSON.stringify(model._params));
        console.info('--------------------\n');
    }

    // TEST 2
    {
        var iter = 4000;
        var lambda = 0.2;
        theta = new Matrix([0,0,0]);

        var model = learning.learningRegression(logistic.computeCostGrad, theta, sample1.X, sample1.Y, lambda, iter);

        console.info("J : " + JSON.stringify(model._costFunction) + ' --- Expected cost (approx): 0.218')
        console.info("Grad : " + JSON.stringify(model._gradient) + ' --- Expected gradients (approx):\n 0.043\n 2.566\n 2.647\n');
        console.info('Theta : ' + JSON.stringify(model._params));
        console.info('--------------------\n');
    }

    // TEST 2
    {
        var lambda = 1;
        var iter = 40000;
        theta = new Matrix([30, -20, 1]);

        var model = learning.learningRegression(logistic.computeCostGrad, theta, sample2.X, sample2.Y, lambda, iter, 0.1);

        console.info("J : " + JSON.stringify(model._costFunction) + ' --- Expected cost (approx): 3.16\n');
        console.info("Grad : " + JSON.stringify(model._gradient) + ' --- Expected 0.3460\n 0.1614\n 0.1948\n 0.2269\n 0.0922\n');
        console.info('Theta : ' + JSON.stringify(model._params));
        console.info('--------------------\n');
    }

    return [];
};