const linear_algebra = require('./algebra/linear-algebra.min.js')(),
    // initialise it
    Vector = linear_algebra.Vector,
    Matrix = linear_algebra.Matrix;

exports.computeCostGrad = (theta, X, Y, lambda = 0) => {

    // hyp = (X.dot(theta.trans())).sigmoid().plusEach(0.001);
    hyp = (X.dot(theta.trans())).sigmoid();
    // console.log("\n Hypothese : " + JSON.stringify(hyp));

    m = Y.rows;

    if (m === 0)
        return [];

    ////////////////////////////////////////////////////

    theta_ = theta.toArray();
    theta_[0][0] = 0;
    theta_reg = new Matrix(theta_);
    theta_t = theta_reg.trans();
    regul_cost = theta_reg.dot(theta_t).mulEach(lambda/(2*m));

    ////////////////////////////////////////////////////

    Y_ = Y.mulEach(-1).plusEach(1);
    hyp_ = hyp.mulEach(-1).plusEach(1);

    // cost = Y.trans().dot(hyp.log());
    // cost_ = Y_.trans().dot(hyp_.log());
    hyp100 = hyp.mulEach(10000).log();
    hyp100_ = hyp_.mulEach(10000).log();
    cost = Y.trans().dot(hyp100.plusEach(-4));
    cost_ = Y_.trans().dot(hyp100_.plusEach(-4));
    J = cost.plus(cost_);
    J = J.mulEach(-1 / m);
    J = J.plus(regul_cost);

    // console.log("\n Cost: "+JSON.stringify(cost))
    // console.log(" Cost_: "+JSON.stringify(cost_))
    // console.log(" J: "+JSON.stringify(J));

    ////////////////////////////////////////////////////

    regul_grad = theta_reg.mulEach(lambda);
    grad = X.trans().dot(hyp.minus(Y)).plus(regul_grad.trans()).mulEach(1 / m);

    // console.log("\n Gradient : " + JSON.stringify(grad));

    return { "J": J, "grad": grad };
}