var Model = require('./model');
// var tf = require('@tensorflow/tfjs');
var tf = require('@tensorflow/tfjs-node');
var projectManager = require('../lib/projectManager');

exports.learningRegression = function (CostGradComputer, theta, X, Y, lambda = 1, iter = 100, alphaRate = .1) {
    var J = [];
    var cost0;
    var J_grad = {
        'J': undefined,
        'grad': undefined
    }

    for (i = 0; i < iter; i++) {
        J_grad = CostGradComputer(theta, X, Y.trans(), lambda);
        J.push(J_grad['J'].data[0]);

        grad = J_grad['grad'].mulEach(alphaRate);
        theta = theta.minus(grad.trans());
    }

    return new Model(J_grad['J'], theta, J_grad['grad']);
}

exports.learningLayers = function (X_train, Y_train, X_test, inputSize, layUnitsIn, layUnitsOut, projectId) {
    const model = tf.sequential();

    /* tf.layers.activation (args) function
    activation ('elu'|'hardSigmoid'|'linear'|'relu'|'relu6'| 'selu'|'sigmoid'|'softmax'|'softplus'|'softsign'|'tanh')*/

    //config for layer
    const config_hidden = {
        inputShape: [inputSize], activation: 'tanh', units: layUnitsIn
    }
    const config_output = { units: layUnitsOut, activation: 'softmax' }

    //defining the hidden and output layer
    const hidden = tf.layers.dense(config_hidden);
    const output = tf.layers.dense(config_output);

    //adding layers to model
    model.add(hidden);
    model.add(output);

    //define an optimizer
    const optimize = tf.train.sgd(0.1);
    //config for model
    const config = { optimizer: optimize, loss: 'categoricalCrossentropy', metrics: ['accuracy'] };
    //compiling the model
    model.compile(config);

    console.log('Model Successfully Compiled');

    //Dummy training data
    //const x_train = tf.tensor([[0.1, 0.5, 0.1], [0.9, 0.3, 0.4], [0.4, 0.5, 0.5], [0.7, 0.1, 0.9]])
    const x_train = tf.tensor(X_train);
    //Dummy training labels
    // const y_train = tf.tensor([[0.2, 0.8], [0.9, 0.10], [0.4, 0.6], [0.5, 0.5]])
    const y_train = tf.tensor(Y_train);
    //Dummy testing data
    // const x_test = tf.tensor([[0.9, 0.1, 0.5]])
    const x_test = tf.tensor(X_train);
    const y_test = tf.tensor(Y_train);

    train_data().then(function () {
        console.log('Training is Complete');
        console.log('Predictions :');
        // model.predict(x_test).print();
        const predictions = model.predict(x_test);
        predictions.print();
        const yPred = predictions.argMax(-1).dataSync();
        console.log(yPred);

        const yTrue = y_test.argMax(-1).dataSync();

        var correct = 0;
        var wrong = 0;
        for (var i = 0; i < yPred.length; i++) {
            if (yPred[i] === yTrue[i]) {
                correct++;
            }
            else {
                wrong++;
            }
        }
        console.log("\n Prediction error rate: " + (wrong / (wrong + correct)));

        //projectManager.saveModel (projectId, 'my-model-1', model);
        model.save('file:///./test.model');

        // https://stackoverflow.com/questions/57917450/how-to-get-set-weights-for-a-supervised-model-in-tensorflow-js
        model.layers.forEach(layer => {
            console.log("\n " + JSON.stringify(layer.getConfig()))
            console.log("\n Kernel")
            layer.getWeights()[0].print();
            console.log("\n Bias")
            layer.getWeights()[1].print();
        })


        // const precision = tf.metrics.precision(yTrue., predictions).dataSync()[0];
        // const recall = tf.metrics.recall(yTrue, predictions).dataSync()[0];

        // correct = tf.equal(tf.argMax(predictions, 1), tf.argMax(y_test, 1))
        // // accuracy = tf.reduce_mean(tf.cast(correct, 'float'));
        // // console.log("Accuracy : " + accuracy);

        // con_mat = tf.confusion_matrix(labels = [1, 2, 3], predictions = correct, num_classes = layUnitsOut, dtype = tf.int32, name = None)
        // console.log("Confusion matrix: " + tf.Tensor.eval(con_mat, feed_dict = None, session = None));
    })

    function onBatchEnd(batch, logs) {
        console.log('Accuracy', logs.acc);
    }


    async function train_data() {
        for (let i = 0; i < 10; i++) {
            const res = await model.fit(x_train, y_train, {epochs: 5,
                batchSize: 32,
                callbacks: {onBatchEnd}
              }).then(info => {
                console.log('Final accuracy', info.history.acc);
              });
            //console.log(res.history.loss[0]);
        }
    }
}
